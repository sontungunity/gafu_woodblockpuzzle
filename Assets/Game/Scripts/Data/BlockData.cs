using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "BlockData", menuName = "Game/BlockData")]
public class BlockData : ScriptableObject {
    public int id;
    public List<Vector2> lstBlockPos;
    
    public Vector2 GetCenter() {
        float x_min = 0,y_min = 0,x_max = 0,y_max = 0;
        foreach(var pos in lstBlockPos) {
            if(pos.x < x_min)
                x_min = pos.x;
            if(pos.x > x_max)
                x_max = pos.x;
            if(pos.y < y_min)
                y_min = pos.y;
            if(pos.y > y_max)
                y_max = pos.y;
        }
        float x_Center = (x_max - x_min)/2f;
        float y_Center = (y_max - y_min)/2f;
        return new Vector2(x_Center,y_Center);
    }
}

public enum BlockAngle {
    ANGLE0 = 0,
    ANGLE_90 = 1,
    ANGLE_180 = 2,
    ANGLE_270 = 3
}
