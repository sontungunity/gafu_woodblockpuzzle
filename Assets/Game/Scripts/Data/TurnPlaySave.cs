using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using STU;

[System.Serializable]
public class TurnPlaySave {
    public int score = 0;
    public bool moreChance = false;
    public bool highScore = true;
    public BoardSave boardSave = new BoardSave();
    public List<BlockSave> LstBlockSave = new List<BlockSave>();
    public void GetDataSave() {
        if(InGameManager.Instance != null) {
            this.score = InGameManager.Instance.m_Score;
        }
    }
}

[System.Serializable]
public class BoardSave {
    public List<CellSave> lstCellSave = new List<CellSave>();
}
[System.Serializable]
public class BlockSave {
    public List<CellSave> lstCell = new List<CellSave>();

    public Vector2 GetCenter() {
        float x_min = 0,y_min = 0,x_max = 0,y_max = 0;
        foreach(var cell in lstCell) {
            if(cell.coor.m_X < x_min)
                x_min = cell.coor.m_X;
            if(cell.coor.m_X > x_max)
                x_max = cell.coor.m_X;
            if(cell.coor.m_Y < y_min)
                y_min = cell.coor.m_Y;
            if(cell.coor.m_Y > y_max)
                y_max = cell.coor.m_Y;
        }
        float x_Center = (x_max + x_min)/2f;
        float y_Center = (y_max + y_min)/2f;
        return new Vector2(x_Center, y_Center);
    }
}

[System.Serializable]
public class CellSave {
    public Coor coor;
    public List<MaterialID> LstMaterialID = new List<MaterialID>();
}
