using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {
    public bool Tutorial = false;
    public int HighScore = 0;
    public List<ItemStack> Eventory = new List<ItemStack>(){ new ItemStack(ItemID.Booter_Swap,3),new ItemStack(ItemID.Booter_Rotation,3),new ItemStack(ItemID.Booter_Bomb,3)};
    public TurnPlaySave TurnPlaySave = new TurnPlaySave();

    public ItemStack GetItemSaveByItemId(ItemID itemID) {
        ItemStack result = Eventory.Find(x => x.ItemID == itemID);
        if(result == null) {
            result = new ItemStack(itemID, 0);
            Eventory.Add(result);
        }
        return result;
    }

    public bool RemoveItem(ItemStack itemStack) {
        ItemStack result = GetItemSaveByItemId(itemStack.ItemID);
        if(result.Amount < itemStack.Amount) {
            return false;
        }
        result.Add(-itemStack.Amount);
        EventDispatcher.Dispatch<EventKey.IteamChange>(new EventKey.IteamChange(itemStack.ItemID, result.Amount, itemStack.Amount));
        return true;
    }

    public bool AddItem(ItemStack itemStack) {
        ItemStack result = GetItemSaveByItemId(itemStack.ItemID);
        result.Add(itemStack.Amount);
        EventDispatcher.Dispatch<EventKey.IteamChange>(new EventKey.IteamChange(itemStack.ItemID, result.Amount, itemStack.Amount));
        return true;
    }
}
