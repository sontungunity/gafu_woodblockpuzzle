using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using System;

public class OneMoreChance : FrameBase
{
    [SerializeField] private TextMeshProUGUI txt_Count;
    [SerializeField] private Button btn_Watch;
    private Tween tween;

    private void Awake() {
        btn_Watch.onClick.AddListener(ButtonWatchAds);
    }
    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        CountDown();
    }

    private void CountDown() {
        tween.CheckKillTween();
        tween = DOTween.To(() => 5,
            (value) => {
                int count = value;
                txt_Count.text = count.ToString();
            },
            0,
            5f).SetEase(Ease.Linear).OnComplete(()=> {
                Hide(()=> {
                    FrameManager.Instance.Push<ResultFrame>();
                });
            });
    }

    private void ButtonWatchAds() {
        Hide(() => {
            InGameManager.Instance.OneMoreChance();
        });
    }

    private void OnDisable() {
        tween.CheckKillTween();
    }

    private void OnDestroy() {
        tween.CheckKillTween();
    }

}
