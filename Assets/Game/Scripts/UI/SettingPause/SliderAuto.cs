using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderAuto : MonoBehaviour {
    [SerializeField] private Button btn_OnSelect;
    [Header("Position")]
    [SerializeField] private Transform transPoint;
    [SerializeField] private Vector3 onPosition;
    [SerializeField] private Vector3 offPosition;
    [Space]
    [SerializeField] private DisplayObjects objStatus;//0.On 1.Off
    [SerializeField] private DisplayObjects objSttBg; //0.On 1.Off

    private bool IsOn;
    private Action<bool> callBack;

    private void Awake() {
        btn_OnSelect.onClick.AddListener(OnClick);
    }

    public void Init(Action<bool> callBack) {
        this.callBack = callBack;
    }

    //private void Start() {
    //    SetUpByStatus(IsOn);
    //}

    public void Show(bool OnStatus) {
        SetUpByStatus(OnStatus);
    }

    private void SetUpByStatus(bool IsOn) {
        this.IsOn = IsOn;
        if(IsOn) {
            transPoint.transform.localPosition = onPosition;
            objStatus.Active(0);
            objSttBg.Active(0);
        } else {
            transPoint.transform.localPosition = offPosition;
            objStatus.Active(1);
            objSttBg.Active(1);
        }
    }

    public void OnClick() {
        SetUpByStatus(!IsOn);
        callBack?.Invoke(IsOn);
    }
}
