using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseFrame : FrameBase {
    [SerializeField] private SliderAuto sli_Music;
    [SerializeField] private SliderAuto sli_Sound;
    [SerializeField] private SliderAuto sli_Vibra;
    [SerializeField] private TextMeshProUGUI txt_Version;
    [SerializeField] private Button btn_Tutorial;
    [SerializeField] private Button btn_ShowRate;
    [SerializeField] private Button btn_Replay;
    [SerializeField] private Button btn_Home;

    private SoundManager soundI => SoundManager.Instance;

    private void Awake() {
        sli_Music.Init(SetUpMusic);
        sli_Sound.Init(SetUpSound);
        sli_Vibra.Init(SetUpVibra);
        //btn_ShowRate.onClick.AddListener(()=> { Hide(); FrameManager.Instance.Push<RatePanel>();});
        btn_Tutorial.onClick.AddListener(() => { });
        btn_ShowRate.onClick.AddListener(() => { });
        btn_Replay.onClick.AddListener(ButtonReplay);
        btn_Home.onClick.AddListener(ButtonHome);
    }

    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        sli_Music.Show(soundI.MusicEnabled);
        sli_Sound.Show(soundI.SoundEnabled);
        sli_Vibra.Show(soundI.VibrateEnabled);
        txt_Version.text = $"Version {Application.version}";
        InGameManager.Instance.Status = InGameManager.GameStatus.PAUSE;
    }

    private void SetUpMusic(bool OnMusic) {
        soundI.MusicEnabled = OnMusic;
    }

    private void SetUpSound(bool OnSound) {
        soundI.SoundEnabled = OnSound;
    }

    private void SetUpVibra(bool OnVibra) {
        soundI.VibrateEnabled = OnVibra;
    }

    private void ButtonReplay() {
        InGameManager.Instance.Replay();
        Hide();
    }

    private void ButtonHome() {
        InGameManager.Instance.SaveTurnPlay();
        SceneManager.Instance.LoadSceneAsyn(SceneManager.SCENE_HOME);
    }

    public override void Hide(Action onCompleted = null, bool instant = false) {
        base.Hide(onCompleted, instant);
        InGameManager.Instance.Status = InGameManager.GameStatus.NORMAL;
    }
}
