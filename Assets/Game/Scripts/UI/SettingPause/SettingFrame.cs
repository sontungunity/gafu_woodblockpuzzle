using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SettingFrame : FrameBase
{
    [SerializeField] private SliderAuto sli_Music;
    [SerializeField] private SliderAuto sli_Sound;
    [SerializeField] private SliderAuto sli_Vibra;
    [SerializeField] private TextMeshProUGUI txt_Version;
    [SerializeField] private Button btn_ShowRate;

    private SoundManager soundI => SoundManager.Instance;

    private void Awake() {
        sli_Music.Init(SetUpMusic);
        sli_Sound.Init(SetUpSound);
        sli_Vibra.Init(SetUpVibra);
        //btn_ShowRate.onClick.AddListener(()=> { Hide(); FrameManager.Instance.Push<RatePanel>();});
    }

    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);

        sli_Music.Show(soundI.MusicEnabled);
        sli_Sound.Show(soundI.SoundEnabled);
        sli_Vibra.Show(soundI.VibrateEnabled);
        txt_Version.text = $"Version {Application.version}";
    }

    private void SetUpMusic(bool OnMusic) {
        soundI.MusicEnabled = OnMusic;
    }

    private void SetUpSound(bool OnSound) {
        soundI.SoundEnabled = OnSound;
    }

    private void SetUpVibra(bool OnVibra) {
        soundI.VibrateEnabled = OnVibra;
    }
}
