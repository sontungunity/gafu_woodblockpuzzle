using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;

public class TextNumberChange : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI txt_Amount;
    private Tween tween;
    public void Show(int amout) {
        int cur_int = 0;
        if(!Int32.TryParse(txt_Amount.text,out cur_int)) {
            return;
        }
        tween.CheckKillTween(true);
        //txt_Amount.transform.localScale = Vector3.one * 1.2f;
        tween = DOTween.To(() => cur_int,
            (value) => {
                txt_Amount.text = value.ToString();
            },
            amout,
            0.5f
            ).OnComplete(() => {
                txt_Amount.text = amout.ToString();
                //txt_Amount.transform.localScale = Vector3.one * 1f;
            });
    }
}
