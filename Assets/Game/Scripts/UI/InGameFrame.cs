using STU;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameFrame : FrameBase
{
    [SerializeField] private TextNumberChange txt_HighScore;
    [SerializeField] private TextNumberChange txt_Score;
    [SerializeField] private BooterManager booterManager;
    [SerializeField] private Button btn_Pause;

    private void Awake() {
        btn_Pause.onClick.AddListener(ButtonPause);
    }

    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.ScoreChange>(EventUpdateScore);
        EventDispatcher.AddListener<EventKey.HightScore>(EventUpdateHighScore);
    }
    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.ScoreChange>(EventUpdateScore);
        EventDispatcher.RemoveListener<EventKey.HightScore>(EventUpdateHighScore);
    }
    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        txt_HighScore.Show(DataManager.Instance.PlayerData.HighScore);
        txt_Score.Show(InGameManager.Instance.m_Score);
        booterManager.Show();
    }

    private void EventUpdateScore(EventKey.ScoreChange evt) {
        txt_Score.Show(InGameManager.Instance.m_Score);
    }

    private void EventUpdateHighScore(EventKey.HightScore evt) {
        txt_HighScore.Show(DataManager.Instance.PlayerData.HighScore);
    }

    private void ButtonPause() {
        FrameManager.Instance.Push<PauseFrame>();
    }
}
