using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HomeFrameSpine : MonoBehaviour
{
    [SerializeField] private List<SkeletonGraphic> lstSkeletonGraphic;
    System.Random rnd;
    private WaitForSeconds waitForSecond;
    private Coroutine coroutine;
    private void Start() {
        rnd = new System.Random();
        waitForSecond = new WaitForSeconds(3f);
        coroutine = StartCoroutine(DoAnim());
    }

    IEnumerator DoAnim() {
        int[] indexRandom = Enumerable.Range(0, lstSkeletonGraphic.Count).OrderBy(r => rnd.Next()).ToArray();
        for(int i = 0; i < indexRandom.Length;i++) {
            var spine = lstSkeletonGraphic[indexRandom[i]];
            spine.AnimationState.SetAnimation(0, "animation", false);
            yield return waitForSecond;
        }
        coroutine = StartCoroutine(DoAnim());
    }
}
