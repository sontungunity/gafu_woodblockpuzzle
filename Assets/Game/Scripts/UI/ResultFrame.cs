using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ResultFrame : FrameBase
{
    [SerializeField] private TextMeshProUGUI txt_Score;
    [SerializeField] private TextMeshProUGUI txt_HighScore;
    [SerializeField] private Button btn_Replay;
    [SerializeField] private Button btn_Home;
    [SerializeField] private AudioClip m_soundLoser;
    [SerializeField] private GameObject banner_HighScore;
    private void Awake() {
        btn_Replay.onClick.AddListener(Replay);
        btn_Home.onClick.AddListener(Home);
    }

    public void SetHighScore(bool highScore) {
        banner_HighScore.SetActive(highScore);
    }

    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        SoundManager.Instance.PlayMusic(m_soundLoser,loop:false);
        txt_Score.text = InGameManager.Instance.m_Score.ToString();
        txt_HighScore.text = "<sprite=\"cup\" name=\"cup\" >" + DataManager.Instance.PlayerData.HighScore.ToString();
    }

    private void Replay() {
        Hide();
        InGameManager.Instance.Replay();
    }

    private void Home() {
        InGameManager.Instance.SaveTurnPlay();
        SceneManager.Instance.LoadSceneAsyn(SceneManager.SCENE_HOME);
    }
}
