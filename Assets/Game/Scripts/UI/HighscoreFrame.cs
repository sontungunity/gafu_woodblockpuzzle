using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreFrame : FrameBase
{
    [SerializeField] private float timeDisplay;
    [SerializeField] private AudioClip audioSoundFireWork;
    //[SerializeField] private ParticleSystem parI,parII;
    private Coroutine coroutine;
    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        InGameManager.Instance.Status = InGameManager.GameStatus.PAUSE;
        SoundManager.Instance.PlaySound(audioSoundFireWork);
        if(coroutine!=null) {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(CountDown());
    }

    IEnumerator CountDown() {
        yield return new WaitForSeconds(timeDisplay);
        Hide();
    }

    public override void Hide(Action onCompleted = null, bool instant = false) {
        base.Hide(onCompleted, instant);
        InGameManager.Instance.Status = InGameManager.GameStatus.NORMAL;
    }

    private void OnEnable() {
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
    }

    private void OnDestroy() {
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
    }
}
