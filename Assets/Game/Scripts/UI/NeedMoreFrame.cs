using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NeedMoreFrame : FrameBase
{
    [SerializeField] private SkeletonGraphic skeleton;
    [SerializeField] private Button btn_ADS,btn_Close;
    private ItemID itemID;

    public void Awake() {
        btn_Close.onClick.AddListener(()=> { Hide(); });
        btn_ADS.onClick.AddListener(ButtonAds);
    }

    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        InGameManager.Instance.Status = InGameManager.GameStatus.PAUSE;
    }

    public void SetUpIDBooter(ItemID itemID) {
        this.itemID = itemID;
        skeleton.AnimationState.SetAnimation(0, itemID.GetDataByID().Description, true);
    }

    private void ButtonAds() {
        Hide(()=> {
            DataManager.Instance.PlayerData.AddItem(new ItemStack(itemID,1));
        });
    }

    public override void Hide(Action onCompleted = null, bool instant = false) {
        base.Hide(onCompleted, instant);
        InGameManager.Instance.Status = InGameManager.GameStatus.NORMAL;
    }
}
