using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeFrame : FrameBase
{
    [SerializeField] private Button m_btnPlay;
    [SerializeField] private AudioClip m_musicHome;
    [SerializeField] private Button btn_userInfo;
    [SerializeField] private Button btn_Challenge;
    [SerializeField] private Button btn_Shop;
    [SerializeField] private Button btn_Mission;
    [SerializeField] private Button btn_Rank;
    [SerializeField] private Button btn_Setting;
    public Button BtnStart => m_btnPlay;
    private void Awake() {
        m_btnPlay.onClick.AddListener(StartGame);
        btn_userInfo.onClick.AddListener(() => { TextNotify.Instance.Show("COMING SOON !!"); });
        btn_Challenge.onClick.AddListener(() => { TextNotify.Instance.Show("COMING SOON !!"); });
        btn_Shop.onClick.AddListener(() => { TextNotify.Instance.Show("COMING SOON !!"); });
        btn_Mission.onClick.AddListener(() => { TextNotify.Instance.Show("COMING SOON !!"); });
        btn_Rank.onClick.AddListener(() => { TextNotify.Instance.Show("COMING SOON !!"); });
        btn_Setting.onClick.AddListener(() => { FrameManager.Instance.Push<SettingFrame>(); });
    }

    public override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        SoundManager.Instance.PlayMusic(m_musicHome);
        if(DataManager.Instance.PlayerData.Tutorial == false) {
            TutorialManager.Instance.ShowTutorial(TutorialID.START);
        }
    }

    private void StartGame() {
        SceneManager.Instance.LoadSceneAsyn(SceneManager.SCENE_GAME);
    }
}
