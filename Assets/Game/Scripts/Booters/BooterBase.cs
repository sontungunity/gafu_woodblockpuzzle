using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using STU;
using System;

public class BooterBase : MonoBehaviour
{
    [SerializeField] protected ItemID idBooster;
    [SerializeField] protected TextMeshProUGUI txt_Amount;
    [SerializeField] protected Button btn_OnSelect;
    public bool BooterActive;
    protected PlayerData playerData => DataManager.Instance.PlayerData;
    private BooterManager booterM;

    public virtual void Awake() {
        btn_OnSelect.onClick.AddListener(OnClick);
    }

    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.IteamChange>(UpDateUI);
        EventDispatcher.AddListener<EventKey.GetItemFromUI>(HanlderItem);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.IteamChange>(UpDateUI);
        EventDispatcher.RemoveListener<EventKey.GetItemFromUI>(HanlderItem);
    }

    public void HanlderItem(EventKey.GetItemFromUI evt) {
        if(evt.ItemStack.ItemID == idBooster) {
            Vector2 endPos = InGameManager.Instance.MainCamera.WorldToScreenPoint(transform.position);
            CollectionController.Instance.GetItemStack(evt.ItemStack, evt.Position, endPos, ()=> {
                DataManager.Instance.PlayerData.AddItem(evt.ItemStack);
            });
        }
    }

    private void UpDateUI(EventKey.IteamChange evt) {
        if(evt.itemID == idBooster) {
            txt_Amount.text = playerData.GetItemSaveByItemId(idBooster).Amount.ToString();
        }
    }

    public void SetOpen(bool open) {
        if(open) {
            btn_OnSelect.gameObject.SetActive(true);
        } else {
            btn_OnSelect.gameObject.SetActive(false);
        }
    }

    public void Init(BooterManager booterM) {
        this.booterM = booterM;
    }

    public virtual void Show() {
        BooterActive = false;
        txt_Amount.text = playerData.GetItemSaveByItemId(idBooster).Amount.ToString();
    }

    private void OnClick() {
        if(BooterActive) {
            OnSelect(false);
        } else {
            if(booterM.IsDoing) {
                return;
            }

            if(playerData.GetItemSaveByItemId(idBooster).Amount<=0) {
                FrameManager.Instance.Push<NeedMoreFrame>().SetUpIDBooter(idBooster);
                return;
            }

            OnSelect(true);
        }
    }

    public void OnSelect(bool select) {
        if(select) {
            OnAction();            
        } else {
            OnFinish();
        }
    }

    public virtual void OnAction() {
        booterM.OnSelect(this);
        BooterActive = true;
    }

    public virtual void OnFinish() {
        BooterActive = false;
        booterM.UnSelect();
    }
    [ContextMenu("TestMoreNeed")]
    public void TestMoreNeed() {
        FrameManager.Instance.Push<NeedMoreFrame>().SetUpIDBooter(idBooster);
    }
}
