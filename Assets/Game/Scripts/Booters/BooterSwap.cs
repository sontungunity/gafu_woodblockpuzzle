using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BooterSwap : BooterBase
{
    public override void OnAction() {
        base.OnAction();
        playerData.RemoveItem(new ItemStack(idBooster,1));
        InGameManager.Instance.SpawnerManager.GetNewBlocks(true);
        OnFinish();
    }
}
