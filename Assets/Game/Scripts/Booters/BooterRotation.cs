using DG.Tweening;
using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BooterRotation : BooterBase {
    [SerializeField]private BooterRotationSupport pref;
    [SerializeField]private Image m_HighlightRenderer;
    private List<BooterRotationSupport> lstPart = new List<BooterRotationSupport>();
    private BlockSpawnerManager spawnerManager => InGameManager.Instance.SpawnerManager;
    public override void Show() {
        base.Show();
        foreach(var part in lstPart) {
            part.gameObject.SetActive(false);
        }
        TurnOnHightLight(false);
    }
    public override void OnAction() {
        base.OnAction();
        for(var i = 0; i < spawnerManager.LstBlockSpawner.Count; i++) {
            BooterRotationSupport part;
            if(i >= lstPart.Count) {
                part = pref.Spawn();
                part.Init(this);
                lstPart.Add(part);
            } else {
                part = lstPart[i];
            }
            part.gameObject.SetActive(true);
            part.Show(spawnerManager.LstBlockSpawner[i]);
        }
        TurnOnHightLight(true);
    }

    public override void OnFinish() {
        base.OnFinish();
        TurnOnHightLight(false);
        bool doRotation = false;
        foreach(var part in lstPart) {
            if(part.DoRotation) {
                doRotation = true;
            }
            part.UnShow();
            part.gameObject.SetActive(false);
        }

        if(doRotation) {
            playerData.RemoveItem(new ItemStack(idBooster, 1));
            EventDispatcher.Dispatch<EventKey.BlockChange>(new EventKey.BlockChange());
        }
    }

    public void TurnOnHightLight(bool turnOn) {
        if(turnOn) {
            m_HighlightRenderer.color = new Color(1, 1, 1, 0);
            m_HighlightRenderer.enabled = true;
            DOTween.Kill("button_booster_highlight");
            Sequence highlightSeq = DOTween.Sequence();
            highlightSeq.SetId("button_booster_highlight");
            highlightSeq.Append(m_HighlightRenderer.DOFade(1f, 1f));
            highlightSeq.Append(m_HighlightRenderer.DOFade(0f, 1f));
            highlightSeq.SetLoops(-1);
        } else {
            DOTween.Kill("button_booster_highlight");
            m_HighlightRenderer.enabled = false;
        }
    }

    public void CheckHasOneBlock() {
        OnFinish();
        EventDispatcher.Dispatch<EventKey.BlockChange>(new EventKey.BlockChange());
        //bool hasOne = false;
        //foreach(var part in lstPart) {
        //    if(part.CanSelect) {
        //        hasOne = true;
        //        break;
        //    }
        //}

        //if(!hasOne) {
        //    OnFinish();
        //    EventDispatcher.Dispatch<EventKey.BlockChange>(new EventKey.BlockChange());
        //}
    }
}
