using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BooterManager : Singleton<BooterManager>
{
    [SerializeField] private List<BooterBase> lstBooter;
    private BooterBase curBooster;

    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.BoardChange>(ListenBoardChange);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.BoardChange>(ListenBoardChange);
    }

    private void ListenBoardChange(EventKey.BoardChange evt) {
        TurnOff();
    }

    private void Awake() {
        foreach(var booter in lstBooter) {
            booter.Init(this);
        }
    }

    public bool IsDoing {
        get {
            foreach(var booter in lstBooter) {
                if(booter.BooterActive) {
                    return true;
                }
            }
            return false;
        }
    }

    public void Show() {
        foreach(var booter in lstBooter) {
            booter.Show();
        }
    }

    public void OnSelect(BooterBase booterBase) {
        this.curBooster = booterBase;
        foreach(var booter in lstBooter) {
            booter.SetOpen(booter == curBooster);
        }
    }

    public void UnSelect() {
        this.curBooster = null;
        foreach(var booter in lstBooter) {
            booter.SetOpen(true);
        }
    }

    public void TurnOff() {
        if(this.curBooster!=null) {
            this.curBooster.OnFinish();
        }
    }
}
