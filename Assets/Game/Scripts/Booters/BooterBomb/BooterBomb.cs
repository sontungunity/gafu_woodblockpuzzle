using DG.Tweening;
using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BooterBomb : BooterBase
{
    [SerializeField]private Image m_HighlightRenderer;
    [SerializeField] private BooterBombSupport  boomPref;
    private BooterBombSupport boomSp;
    private BlockSpawnerManager spawner => InGameManager.Instance.SpawnerManager;
    private Board board => InGameManager.Instance.Board;
    public override void Awake() {
        base.Awake();
        if(boomSp == null) {
            boomSp = Instantiate(boomPref);
        }
        boomSp.Init(this);
        boomSp.gameObject.SetActive(false);
    }

    public override void Show() {
        base.Show();
        TurnOnHightLight(false);
    }

    public override void OnAction() {
        base.OnAction();
        foreach(var spanwer in spawner.LstBlockSpawner) {
            spawner.gameObject.SetActive(false);
        }
        boomSp.gameObject.SetActive(true);
        boomSp.transform.position = spawner.transform.position;
        boomSp.SetUpdefaul();
        TurnOnHightLight(true);
    }

    public override void OnFinish() {
        base.OnFinish();
        boomSp.gameObject.SetActive(false);
        foreach(var spanwer in spawner.LstBlockSpawner) {
            spawner.gameObject.SetActive(true);
        }

        if(!boomSp.CanSelect) {
            DataManager.Instance.PlayerData.RemoveItem(new ItemStack(idBooster,1));
        }
        TurnOnHightLight(false);
    }

    public void TurnOnHightLight(bool turnOn) {
        if(turnOn) {
            m_HighlightRenderer.color = new Color(1, 1, 1, 0);
            m_HighlightRenderer.enabled = true;
            DOTween.Kill("button_booster_highlight");
            Sequence highlightSeq = DOTween.Sequence();
            highlightSeq.SetId("button_booster_highlight");
            highlightSeq.Append(m_HighlightRenderer.DOFade(1f, 1f));
            highlightSeq.Append(m_HighlightRenderer.DOFade(0f, 1f));
            highlightSeq.SetLoops(-1);
        } else {
            DOTween.Kill("button_booster_highlight");
            m_HighlightRenderer.enabled = false;
        }
    }
}
