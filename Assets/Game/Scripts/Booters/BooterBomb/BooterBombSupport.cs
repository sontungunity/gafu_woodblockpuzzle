using DG.Tweening;
using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
public class BooterBombSupport : MonoBehaviour, IPlayerInput {
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject border;
    [SerializeField] private AudioClip sound_Select,sound_DropFail,soundBomb;
    [SerializeField] private GameObject boomEffect;
    [SerializeField] private Vector2 deltaMove;
    [SerializeField] private float speed,timeTarget;
    [SerializeField] private SortingGroup sortingGroup;
    private Board m_Board => InGameManager.Instance.Board;
    public bool CanSelect => !Used;

    public Transform I_Transform => transform;
    private BooterBomb booterBoom;
    private Vector3 startPosition;
    private Vector3 targetPosition;
    private StatusMove sttMove = StatusMove.NON_MOVE;
    private Tween tween;
    public bool Used = false;
    public void Init(BooterBomb booterBoom) {
        this.booterBoom = booterBoom;
    }

    public void SetUpdefaul() {
        sortingGroup.enabled = false;
        bomb.transform.localPosition = Vector3.zero;
        bomb.gameObject.SetActive(true);
        border.gameObject.SetActive(false);
        lstCell_Break.Clear();
        sttMove = StatusMove.NON_MOVE;
        Used = false;
    }

    public bool HandleDown(Vector2 positionWorld) {
        sortingGroup.enabled = true;
        SoundManager.Instance.PlaySound(sound_Select);
        startPosition = positionWorld;
        targetPosition = positionWorld + deltaMove;
        sttMove = StatusMove.MOVE_TO;
        tween = bomb.transform.DOMove(targetPosition, 0.5f).SetEase(Ease.Linear).OnComplete(()=> {
            tween = null;
        });
        return true;
    }
    private Cell cellCenter = null;
    private List<Cell> lstCell_Break = new List<Cell>();
    public void HandleMove(Vector2 positionWorld) {
        if(tween != null) {
            float distan = Vector3.Distance(startPosition,positionWorld);
            if(distan > 0.2f) {
                tween.CheckKillTween();
                tween = null;
            }
        }
        targetPosition = positionWorld + deltaMove;
    }

    public void HandleUp(Vector2 positionWorld) {
        sttMove = StatusMove.NON_MOVE;
        if(lstCell_Break.Count > 0) {
            Used = true;
            m_Board.TurnOnLight(lstCell_Break, false);
            foreach(var cell in lstCell_Break) {
                foreach(var material in cell.LstMaterial) {
                    var iboom = material.GetComponent<IBooterBomb>();
                    if(iboom != null) {
                        iboom.TargetBoom(bomb.transform.position);
                    } else {
                        material.ClearMaterial(true);
                        material.Recycle();
                    }
                }
                cell.LstMaterial.Clear();
            }
            //bomb.gameObject.SetActive(false);
            MakeExplo(bomb.transform.position);
            bomb.transform.localPosition = Vector3.zero;
            EventDispatcher.Dispatch<EventKey.BoardChange>(new EventKey.BoardChange());
        } else {
            bomb.gameObject.SetActive(true);
            border.gameObject.SetActive(false);
            lstCell_Break.Clear();

            tween.CheckKillTween();
            float distan = Vector3.Distance(bomb.transform.localPosition,Vector3.zero);
            float time = distan/speed;
            tween = bomb.transform.DOLocalMove(Vector3.zero, time).SetEase(Ease.Linear).OnComplete(() => {
                tween = null;
                SoundManager.Instance.PlaySound(sound_DropFail);
            });
        }
    }

    private void MakeExplo(Vector3 position) {
        SoundManager.Instance.PlaySound(soundBomb);
        SoundManager.Instance.Vibrate();
        EventDispatcher.Dispatch<EventKey.Vibration>(new EventKey.Vibration(2));
        var bomb =  boomEffect.Spawn();
        bomb.transform.position = position;
        bomb.gameObject.name = "Bomb Use";
        bomb.GetComponent<ParticleSystem>().Play();
    }

    private void LateUpdate() {
        if(sttMove == StatusMove.NON_MOVE) {
            return;
        }

        if(sttMove == StatusMove.MOVE_TO) {
            if(tween == null) {
                float distance = Time.deltaTime * speed;
                bomb.transform.position = Vector3.MoveTowards(bomb.transform.position, targetPosition, distance);
                m_Board.TurnOnLight(lstCell_Break, false);
                border.gameObject.SetActive(false);
                lstCell_Break.Clear();
                cellCenter = m_Board.GetCellByPosition(bomb.transform.position);
                if(cellCenter != null) {
                    int index_x_Start = cellCenter.Coor.m_X - 2;
                    int index_y_Start = cellCenter.Coor.m_Y -2;
                    for(int x = 0; x < 5; x++) {
                        for(int y = 0; y < 5; y++) {
                            var gridElement = m_Board.GetCellByIndex(x+index_x_Start,y+index_y_Start);
                            if(gridElement != null && gridElement.HasMaterial == true) {
                                lstCell_Break.Add(gridElement);
                            }
                        }
                    }
                }
                m_Board.TurnOnLight(lstCell_Break, true);
                border.gameObject.SetActive(lstCell_Break.Count > 0);
            }
        } 
        //else if(sttMove == StatusMove.MOVE_BACK) {
        //    float distance = Time.deltaTime * speed;
        //    bomb.transform.position = Vector3.MoveTowards(bomb.transform.position, targetPosition, distance);
        //    if(Vector3.Distance(bomb.transform.position, targetPosition) < 0.1f) {
        //        sttMove = StatusMove.NON_MOVE;
        //        bomb.transform.localPosition = Vector3.zero;
        //        //bomb.OnSelect(false);
        //        SoundManager.Instance.PlaySound(sound_DropFail);
        //    }
        //}

    }
}
