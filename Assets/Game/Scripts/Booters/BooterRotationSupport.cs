﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BooterRotationSupport : MonoBehaviour, IPlayerInput {
    [SerializeField] private float distanMove = 0.2f;
    [SerializeField] private float timeHold = 0.5f;
    [SerializeField] private AudioClip soundRotation;
    private BlockSpawner blockSpawner;
    private float angle = 0;
    public bool DoRotation {
        get {
            return angle % 360 != 0;
        }
    }

    public Transform I_Transform => transform;

    public bool CanSelect => blockSpawner.CanSelect;
    private bool isSelected = false;
    private float timeSelect = 0;
    private bool isMoveBlock = false;
    private Vector3 startPosition;
    private BooterRotation booterP;

    public void Init(BooterRotation booterP) {
        this.booterP = booterP;
    }

    public bool HandleDown(Vector2 positionWorld) {
        isSelected = true;
        timeSelect = 0;
        isMoveBlock = false;
        startPosition = positionWorld;
        return true;
    }

    public void HandleMove(Vector2 positionWorld) {
        if(isMoveBlock) {
            blockSpawner.HandleMove(positionWorld);
        } else {
            float distance = Vector2.Distance(startPosition,positionWorld);
            if(distance >= distanMove) {
                isMoveBlock = true;
                blockSpawner.HandleDown(positionWorld);
            }
        }
    }

    public void HandleUp(Vector2 positionWorld) {
        isSelected = false;
        if(isMoveBlock) {
            blockSpawner.HandleUp(positionWorld);
            //booterP.CheckHasOneBlock();
        } else {
            blockSpawner.Block.Rotation(-90f);
            SoundManager.Instance.PlaySound(soundRotation);
            angle += -90f;
        }
    }

    public void Show(BlockSpawner blockSpawner) {
        this.blockSpawner = blockSpawner;
        angle = 0;
        Vector3 posBlock = blockSpawner.transform.position;
        posBlock.z = posBlock.z - 2; //cho booterRotation ở trên block
        transform.position = posBlock;
    }

    public void UnShow() {
        angle = 0;
    }

    private void LateUpdate() {
        if(isSelected && !isMoveBlock) {
            timeSelect += Time.deltaTime;
            if(timeSelect > timeHold) {
                isMoveBlock = true;
                blockSpawner.HandleDown(startPosition);
            }
        }
    }
}
