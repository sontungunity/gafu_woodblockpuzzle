using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRecycle : MonoBehaviour
{
    [SerializeField] private float timeRecycle;
    private void OnEnable() {
        StartCoroutine(IERycle());    
    }
    
    IEnumerator IERycle() {
        yield return new WaitForSeconds(timeRecycle);
        gameObject.Recycle();
    }
}
