using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using System;
using System.Linq;

public class ChestManager : MonoBehaviour {
    private static ItemID ITEM_STAR = ItemID.STAR;
    [SerializeField] private int startForOne;
    [SerializeField] private Transform f_CurBar;
    [SerializeField] private RectTransform f_star;
    [SerializeField] private DisplayObjects f_chest; // 0 . Close 1. Open
    [SerializeField] private float f_speed_move;
    [SerializeField] private float f_speed_back;
    [SerializeField] private TextMeshProUGUI txt_Amount;
    [SerializeField] private Button btn_Open,btn_Close;
    [SerializeField] private GameObject txt_Tip;
    [SerializeField] private GameObject chestHighLigh;
    private ItemStack p_itemStar;
    private float p_lineOfStar;
    Sequence p_sequence;
    private Tween tweenProcess;
    private Tween tweenOpenChest;
    private List<ItemID> lstBooster = new List<ItemID>();
    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.GetItemFromUI>(HanlderItem);
    }
    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.GetItemFromUI>(HanlderItem);
        tweenProcess.CheckKillTween();
        tweenOpenChest.CheckKillTween();
    }

    private void HanlderItem(EventKey.GetItemFromUI evt) {
        if(evt.ItemStack.ItemID == ITEM_STAR) {
            Vector2 endPos = InGameManager.Instance.MainCamera.WorldToScreenPoint(f_chest.transform.position);
            CollectionController.Instance.GetItemStack(evt.ItemStack, evt.Position, endPos, UpdateChest);
        }
    }

    private void Awake() {
        //btn_Open.onClick.AddListener(GetReward);
        btn_Close.onClick.AddListener(GetTip);
    }


    private void Start() {
        //Get list booter can get
        List<ItemID> lst = Enum.GetValues(typeof(ItemID)).Cast<ItemID>().ToList();
        foreach(ItemID id in lst) {
            if((int)id >= 10 && (int)id <= 20) {
                lstBooster.Add(id);
            }
        }


        p_sequence = DOTween.Sequence();
        txt_Tip.gameObject.SetActive(false);
        p_itemStar = ITEM_STAR.GetSaveByID();
        int curStar = p_itemStar.Amount % startForOne;
        float curPercent = (float)curStar/startForOne;
        f_CurBar.localScale = new Vector3(curPercent, 1, 1);
        p_lineOfStar = f_star.parent.GetComponent<RectTransform>().sizeDelta.x;
        f_star.anchoredPosition = new Vector3(p_lineOfStar * curPercent, 0, 0);
        UpdateStatusChest();
    }

    private void UpdateChest() {
        p_itemStar = ITEM_STAR.GetSaveByID();
        int curStar = p_itemStar.Amount % startForOne;
        float curPercent = (float)curStar/startForOne;
        float timeChange = 0;
        if(curPercent > f_CurBar.localScale.x) {
            timeChange = (curPercent - f_CurBar.localScale.x) * f_speed_move;
        } else {
            timeChange = (f_CurBar.localScale.x - curPercent) * f_speed_back;
        }
        if(p_sequence != null) {
            p_sequence.Kill();
        }
        p_sequence = DOTween.Sequence();
        p_sequence.Append(
                f_CurBar.DOScaleX(curPercent, timeChange).SetEase(Ease.Linear)
        );
        ;

        p_sequence.Insert(0,
                f_star.DOAnchorPos(new Vector3(p_lineOfStar * curPercent, 0, 0), timeChange).SetEase(Ease.Linear)
        );

        p_sequence.OnComplete(() => {
            UpdateStatusChest();
        });
    }

    private void UpdateStatusChest() {
        p_itemStar = ITEM_STAR.GetSaveByID();
        int amount = Mathf.FloorToInt(p_itemStar.Amount/(float)startForOne);
        txt_Amount.text = amount.ToString();
        f_chest.transform.localScale = Vector3.one;
        f_chest.Active(0);
        if(amount > 0) {
            GetReward();
        }
    }

    private void GetReward() {
        DataManager.Instance.PlayerData.RemoveItem(new ItemStack(ItemID.STAR, startForOne));
        int indexRandom = UnityEngine.Random.Range(0,lstBooster.Count);
        tweenOpenChest.CheckKillTween();
        tweenOpenChest = f_chest.transform.DOScaleY(1.1f, 0.2f).SetLoops(10, LoopType.Yoyo).OnComplete(() => {
            f_chest.transform.localScale = Vector3.one;
            f_chest.Active(1);
            chestHighLigh.transform.localScale = Vector3.zero;
            tweenOpenChest = chestHighLigh.transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutBack).OnComplete(() => {
                ItemID itemID = lstBooster[indexRandom];
                Vector2 startPos = InGameManager.Instance.MainCamera.WorldToScreenPoint(btn_Open.transform.position);
                EventDispatcher.Dispatch<EventKey.GetItemFromUI>(new EventKey.GetItemFromUI(new ItemStack(itemID, 1), startPos));
                tweenOpenChest = chestHighLigh.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBack).OnComplete(() => {
                    f_chest.Active(0);
                });
            });
        });
    }

    private void GetTip() {
        txt_Tip.gameObject.SetActive(true);
        tweenProcess.CheckKillTween();
        tweenProcess = DOVirtual.DelayedCall(2f, () => {
            txt_Tip.gameObject.SetActive(false);
        });
    }

    private void OnDestroy() {
        tweenProcess.CheckKillTween();
    }
}
