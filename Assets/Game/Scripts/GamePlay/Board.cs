﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using STU;

public class Board : MonoBehaviour {
    [SerializeField] private int boardSize = 10;
    [SerializeField] private int cellSize = 1;
    [SerializeField] private Transform root;
    [SerializeField] private Cell  cellPref;
    [SerializeField] private AudioClip sound_normal,sound_cool,sound_excell;
    private Cell[,] arrayCell;
    private Coroutine coroutine;
    public bool GetCombo3;
    private void Awake() {
        arrayCell = new Cell[boardSize, boardSize];
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                Cell cell = Instantiate(cellPref);
                cell.Coor = new Coor(x, y);
                cell.transform.SetParent(root);
                cell.transform.localPosition = GetLocalPositionByGridIndex(x, y);
                cell.name = $"Cell_x{x}_y{y}";
                arrayCell[x, y] = cell;
            }
        }
    }

    public void Show(BoardSave boardSave = null) {
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                arrayCell[x, y].SetUpDefaul();
            }
        }
        BoardSave boarData = boardSave;
        if(boarData == null) {
            boarData = new BoardSave();
        }

        foreach(var cellS in boarData.lstCellSave) {
            var cell = GetCellByIndex(cellS.coor.m_X, cellS.coor.m_Y);
            if(cell != null) {
                cell.Add(cellS.LstMaterialID.ToArray());
            }
        }
        GetCombo3 = false;
    }

    public bool CheckSpace(List<Coor> vector2s, out List<Coor> result_LstPlance, out int result_Option) {
        result_LstPlance = new List<Coor>();
        result_Option = 0;
        bool available = false;
        List<Coor> lstPlanceCheck = new List<Coor>();
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                Cell cell = arrayCell[x, y];
                if(cell.HasMaterial == false) {
                    bool validGridCoord = true;
                    lstPlanceCheck.Clear();
                    for(int i = 0; i < vector2s.Count; i++) {
                        int coordX = x + vector2s[i].m_X;
                        int coordY = y + vector2s[i].m_Y;
                        if(coordX < 0 || coordX >= boardSize || coordY < 0 || coordY >= boardSize) {
                            validGridCoord = false;
                            break;
                        }

                        if(arrayCell[coordX, coordY].HasMaterial) {
                            validGridCoord = false;
                            break;
                        }
                        lstPlanceCheck.Add(new Coor(coordX, coordY));
                    }

                    if(validGridCoord) {
                        available = true;
                        result_Option++;
                        result_LstPlance.Clear();
                        result_LstPlance.AddRange(lstPlanceCheck);
                        if(result_Option > 1) {
                            return available;
                        }
                    } else {
                        result_LstPlance.Clear();
                        continue;
                    }
                }
            }
        }
        return available;
    }
    //private Rect
    private Vector2 GetLocalPositionByGridIndex(int x, int y) {
        float startPosX = -boardSize / 2f + 0.5f;
        float startPosY = -boardSize / 2f + 0.5f;
        return new Vector2(startPosX + x * cellSize, startPosY + y * cellSize);
    }

    public void SetupForBlock(Block block, List<Cell> lstCell_Plance, List<int> lstCol_Break, List<int> lstRow_Break) {
        bool newPance = false;
        bool canPlance = CheckPlance(block,lstCell_Plance,out newPance);
        if(canPlance) {
            if(newPance) {
                lstCol_Break.Clear();
                lstRow_Break.Clear();
                GetBreakCell(lstCell_Plance, lstCol_Break, lstRow_Break);
            }
        } else {
            lstCol_Break.Clear();
            lstRow_Break.Clear();
        }
    }


    private bool CheckPlance(Block block, List<Cell> lstCell_Plance, out bool newPlance) {
        newPlance = true;
        int i = 0;
        foreach(var blockE in block.LstCell) {
            Cell cell = GetCellByPosition(blockE.transform.position);
            if(cell != null && !cell.HasMaterial) {
                if(i == 0 && lstCell_Plance.Count > 0) { // Neu element dau tien trung nhau thi khong can chay nua
                    if(lstCell_Plance[0] == cell && cell != null) {
                        newPlance = false;
                        return true;
                    } else {
                        lstCell_Plance.Clear();
                    }
                }
                lstCell_Plance.Add(cell);
            } else {
                lstCell_Plance.Clear();
                return false;
            }
            i++;
        }
        bool result = lstCell_Plance.Count == block.LstCell.Count();
        if(result) {
            TurnOnLight(lstCell_Plance, true);
        }
        return result;
    }

    private void GetBreakCell(List<Cell> lstCell_Plance, List<int> lstCol_Break, List<int> lstRow_Break) {
        lstCol_Break.Clear();
        //Check colum
        for(int col = 0; col < arrayCell.GetLength(0); col++) {
            bool canBreakCol = true;
            for(int row = 0; row < arrayCell.GetLength(1); row++) {
                var grid = arrayCell[col,row];
                if(!lstCell_Plance.Contains(grid) && grid.HasMaterial == false) {
                    canBreakCol = false;
                    break;
                }
            }
            if(canBreakCol) {
                lstCol_Break.Add(col);
            }
        }
        lstRow_Break.Clear();
        for(int row = 0; row < arrayCell.GetLength(1); row++) {
            bool canBreakRow = true;
            for(int col = 0; col < arrayCell.GetLength(0); col++) {
                var grid = arrayCell[col,row];
                if(!lstCell_Plance.Contains(grid) && grid.HasMaterial == false) {
                    canBreakRow = false;
                    break;
                }
            }
            if(canBreakRow) {
                lstRow_Break.Add(row);
            }
        }
    }

    public void PlanceBlock(Block block, List<Cell> lstCell_Plance, List<int> lstCol_Break, List<int> lstRow_Break, Action callback = null) {
        if(block != null && block.LstCell.Count() == lstCell_Plance.Count()) {
            int score = GetScore(block,lstCol_Break,lstRow_Break);
            InGameManager.Instance.AddScore(score);
            int i = 0;
            foreach(var cell in block.LstCell) {
                lstCell_Plance[i].GetFromOrtherCell(cell);
                i++;
            }
            block.SetUpDefaul();
            int amount = lstCol_Break.Count() + lstRow_Break.Count();
            if(amount == 0) {
                EventDispatcher.Dispatch<EventKey.BoardChange>(new EventKey.BoardChange());
                callback?.Invoke();
            } else {
                if(amount >= 3) {
                    GetCombo3 = true;
                }
                StartCoroutine(BreakGrid(lstCol_Break, lstRow_Break, () => {
                    callback?.Invoke();
                }));
            }
        }
    }

    private int GetScore(Block block, List<int> lstCol_Break, List<int> lstRow_Break) {
        int amountCombo = lstCol_Break.Count + lstRow_Break.Count;
        int result = 0;
        result += amountCombo * 10;
        result += GetBonusCombo(amountCombo);
        result += block.LstCell.Count();
        return result;
    }

    public IEnumerator BreakGrid(List<int> lstCol_Break, List<int> lstRow_Break, Action callback = null) {
        int totalCombo = lstCol_Break.Count +lstRow_Break.Count;
        if(totalCombo == 0) {
            yield return null;
            callback?.Invoke();
        } else {
            TextNotify.Instance.ShowTextByCombo(totalCombo);
            if(totalCombo <= 2) {
                SoundManager.Instance.PlaySound(sound_normal);
            } else if(totalCombo < 4) {
                SoundManager.Instance.Vibrate();
                SoundManager.Instance.PlaySound(sound_cool);
                EventDispatcher.Dispatch<EventKey.Vibration>(new EventKey.Vibration(2));
            } else {
                SoundManager.Instance.Vibrate();
                SoundManager.Instance.PlaySound(sound_excell);
                EventDispatcher.Dispatch<EventKey.Vibration>(new EventKey.Vibration(3));
            }
            bool finishCol = false;
            if(lstCol_Break.Count == 0) {
                finishCol = true;
            } else {
                for(int i = 0; i < lstCol_Break.Count; i++) {
                    if(i != lstCol_Break.Count - 1) {
                        StartCoroutine(IEGetGrid(arrayCell.GetColumn(lstCol_Break[i]).ToList(), false));
                    } else {
                        StartCoroutine(IEGetGrid(arrayCell.GetColumn(lstCol_Break[i]).ToList(), false, () => {
                            finishCol = true;
                        }));
                    }
                }
            }
            bool finishRow = false;
            if(lstRow_Break.Count == 0) {
                finishRow = true;
            } else {
                for(int i = 0; i < lstRow_Break.Count; i++) {
                    if(i != lstRow_Break.Count - 1) {
                        StartCoroutine(IEGetGrid(arrayCell.GetRow(lstRow_Break[i]).ToList()));
                    } else {
                        StartCoroutine(IEGetGrid(arrayCell.GetRow(lstRow_Break[i]).ToList(), callback: () => {
                            finishRow = true;
                        }));
                    }
                }
            }
            yield return new WaitUntil(() => finishCol && finishRow);
            EventDispatcher.Dispatch<EventKey.BoardChange>(new EventKey.BoardChange());
            callback?.Invoke();
        }
    }

    IEnumerator IEGetGrid(List<Cell> lstCell, bool desce = true, Action callback = null) {
        var time = new WaitForSeconds(0.05f);
        if(!desce) {
            lstCell.Reverse();
        }

        foreach(var cell in lstCell) {
            cell.ClearMaterial();
            yield return null;
        }
        callback?.Invoke();
    }

    [ContextMenu("DoHandleLose")]
    public void DoHandleLose(Action callback) {
        coroutine = StartCoroutine(IEDoHandleLose(callback));
    }

    IEnumerator IEDoHandleLose(Action callback) {
        System.Random rnd = new System.Random();
        var arrayX = Enumerable.Range(0, boardSize).OrderBy(r => rnd.Next()).ToArray();
        var arrayY = Enumerable.Range(0, boardSize).OrderBy(r => rnd.Next()).ToArray();
        var delay = new WaitForSeconds(0.01f);
        bool finishBoard = false;
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                Cell cell = arrayCell[arrayX[x], arrayY[y]];
                if(cell.HasMaterial) {
                    cell.SetStatus(Cell.Status.DARK);
                    yield return delay;
                }
                if(x == boardSize - 1 && y == boardSize - 1) {
                    finishBoard = true;
                }
            }
        }

        bool finishTextNotify = false;
        TextNotify.Instance.ShowNoMoreMoves(()=> {
            finishTextNotify = true;
        });
        yield return new WaitUntil(() => finishBoard&&finishTextNotify);
        callback?.Invoke();
    }

    public void SetStatusCell(Cell.Status status) {
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                Cell cell = arrayCell[x, y];
                if(cell.HasMaterial) {
                    cell.SetStatus(Cell.Status.NORMAL);
                }
            }
        }
    }
    #region Support

    #region OnOffLight
    public void TurnOnLight(bool turnOn, params List<Cell>[] arrayList) {
        foreach(var lstCell in arrayList) {
            TurnOnLight(lstCell, turnOn);
        }
    }

    public void TurnOnLight(List<Cell> lstCell, bool turnOn) {
        foreach(var cell in lstCell) {
            cell.SetStatus(turnOn ? Cell.Status.LIGHT : Cell.Status.NORMAL);
        }
    }
    #endregion

    public Cell GetCellByPosition(Vector3 position) {
        position = root.InverseTransformPoint(position);
        int x =  Mathf.FloorToInt(position.x + boardSize/2f);
        int y = Mathf.FloorToInt(position.y + boardSize/2f);
        return GetCellByIndex(x, y);
    }

    public Cell GetCellByIndex(int x, int y) {
        if(x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
            return arrayCell[x, y];
        }
        return null;
    }
    #endregion



    private int GetBonusCombo(int combo) {
        if(combo == 3) {
            return 300;
        } else if(combo == 4) {
            return 500;
        } else if(combo == 5) {
            return 800;
        } else if(combo == 6) {
            return 1300;
        } else if(combo > 6) {
            return 2000;
        }
        return 0;
    }

    public List<Cell> GetGridElementByAxits(int index, Axis axis) {
        if(axis == Axis.COL) {
            return arrayCell.GetColumn(index).ToList();
        } else {
            return arrayCell.GetRow(index).ToList();
        }
    }

    public enum Axis {
        COL,
        ROW
    }

    public BoardSave GetBoardSave() {
        BoardSave result = new BoardSave();
        for(int x = 0; x < boardSize; x++) {
            for(int y = 0; y < boardSize; y++) {
                Cell cell = arrayCell[x, y];
                if(cell!=null && cell.HasMaterial) {
                    CellSave cellS = new CellSave();
                    cellS.coor = new Coor(cell.Coor);
                    foreach(var mar in cell.LstMaterial) {
                        cellS.LstMaterialID.Add(mar.ID);
                    }
                    result.lstCellSave.Add(cellS);
                }
            }
        }
        return result;
    }
}
