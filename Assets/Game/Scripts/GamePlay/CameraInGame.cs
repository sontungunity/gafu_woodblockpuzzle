using DG.Tweening;
using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInGame : MonoBehaviour
{
    [SerializeField] private Camera m_Camera;
    private Vector3 oriPostion;
    private Tween tween;
    private void Awake() {
        oriPostion = transform.position;
    }

    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.Vibration>(HandleShakeCamera);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.Vibration>(HandleShakeCamera);
    }

    private void HandleShakeCamera(EventKey.Vibration evt) {
        int level = evt.level;
        if(level <= 1)
            return;
        if(tween!=null) {
            tween.Kill();
        }
        if(level == 2)
            tween = m_Camera.DOShakePosition(0.3f, new Vector3(0.1f, 0.1f, 0), 20, 0).OnComplete(()=> { m_Camera.transform.position = oriPostion;});
        else if(level == 3)
            tween = m_Camera.DOShakePosition(0.3f, new Vector3(0.115f, 0.115f, 0), 25, 0).OnComplete(() => { m_Camera.transform.position = oriPostion;});
        else
            tween = m_Camera.DOShakePosition(0.3f, new Vector3(0.13f, 0.13f, 0), 30, 0).OnComplete(() => { m_Camera.transform.position = oriPostion;});
    }
}