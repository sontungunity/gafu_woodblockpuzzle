using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using STU;

public class MaterialStar : MaterialBase
{
    public override void ClearMaterial(bool getMaterial) {
        base.ClearMaterial(getMaterial);
        gameObject.Recycle();
        if(getMaterial) {
            ItemStack itemStar = new ItemStack(ItemID.STAR,1);
            DataManager.Instance.PlayerData.AddItem(itemStar);
            Vector2 startPosition = InGameManager.Instance.MainCamera.WorldToScreenPoint(transform.position);
            EventDispatcher.Dispatch<EventKey.GetItemFromUI>(new EventKey.GetItemFromUI(itemStar, startPosition));
        }
    }
}
