using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialBase : MonoBehaviour {
    [SerializeField] private MaterialID id;
    [SerializeField] private SpriteRenderer disPlay;
    [SerializeField] private Sprite sprite_Normal,sprite_DisAble;
    public MaterialID ID => id;
    public virtual void ClearMaterial(bool getMaterial) {
        transform.parent = null;
    }

    public void SetOrderLayer(int order) {
        disPlay.sortingOrder = order;
    }

    public void Show(StatusMaterial status) {
        if(status == StatusMaterial.DISABLE) {
            disPlay.sprite = sprite_DisAble;
        } else {
            disPlay.sprite = sprite_Normal;
        }
    }

    public enum StatusMaterial {
        NORMAL,
        DISABLE,
    }
}


