
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialBlock : MaterialBase, IBooterBomb {
    private Coroutine coroutine;
    private Sequence breakSequence;

    public override void ClearMaterial(bool getMaterial) {
        base.ClearMaterial(getMaterial);
        StartCoroutine(YieldBreak());
    }

    private IEnumerator YieldBreak() {
        transform.rotation = default(Quaternion);
        float currentScale = transform.localScale.x;
        SetOrderLayer(100);
        float randomTargetRotateZ = Random.Range(-270, 270);
        float randomTargetPositionX = Random.Range(-4f, 4f);
        float randomHeightY = Random.Range(2f, 3f);
        if(breakSequence == null) {
            breakSequence = DOTween.Sequence();
        }
        breakSequence.Insert(0, transform.DOScale(currentScale * 1.1f, 0.2f));
        breakSequence.Insert(0, transform.DORotate(new Vector3(0f, 0f, randomTargetRotateZ), 1.2f, RotateMode.FastBeyond360));
        breakSequence.Insert(0, transform.DOMoveY(randomHeightY + transform.position.y, 0.4f).SetEase(Ease.OutCubic));
        breakSequence.Insert(0.4f, transform.DOMoveY(-20f + transform.position.y, 0.8f).SetEase(Ease.InQuad));
        breakSequence.Insert(0, transform.DOMoveX(randomTargetPositionX + transform.position.x, 1.2f).SetEase(Ease.Linear));
        yield return new WaitForSeconds(1.2f);
        breakSequence.Kill();
        breakSequence = null;
        transform.localScale = Vector3.one;
        transform.eulerAngles = Vector3.zero;
        gameObject.Recycle();
    }

    public void TargetBoom(Vector2 position) {
        coroutine = StartCoroutine(YieldBreakByBomb(position));
    }

    private IEnumerator YieldBreakByBomb(Vector3 explodingPoint) {
        if(breakSequence == null) {
            breakSequence = DOTween.Sequence();
        }
        transform.rotation = default(Quaternion);
        float distance = Vector3.Distance(transform.position,explodingPoint);
        Vector3 direction = Vector3.up*20;
        if(distance>0.1f) {
            direction = (transform.position - explodingPoint).normalized*20;
        } 
        float randomTargetRotateZ = Random.Range(-270, 270);
        //float randomTargetPositionX = Random.Range(-1f, 1f) + direction.x;
        //float randomHeightY = Random.Range(2f, 3f) + direction.y;
        breakSequence.Insert(0, transform.DOScale(1.1f, 0.2f));
        breakSequence.Insert(0, transform.DORotate(new Vector3(0f, 0f, randomTargetRotateZ), 2f, RotateMode.FastBeyond360));
        //breakSequence.Insert(0, transform.DOMoveY(randomHeightY + transform.position.y, 1.2f).SetEase(Ease.OutCubic));
        breakSequence.Insert(0, transform.DOMove(direction + transform.position, 2f).SetEase(Ease.OutCirc));
        yield return breakSequence.WaitForCompletion();
        breakSequence.Kill();
        breakSequence = null;
        transform.localScale = Vector3.one;
        transform.eulerAngles = Vector3.zero;
        gameObject.Recycle();
    }




    private void OnDisable() {
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
        if(breakSequence != null) {
            breakSequence.Kill();
        }
    }

    private void OnDestroy() {
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
        if(breakSequence != null) {
            breakSequence.Kill();
        }
    }
}
