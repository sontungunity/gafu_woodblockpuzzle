using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Coor 
{
    public int m_X;
    public int m_Y;

    public Coor(int x = 0,int y = 0) {
        this.m_X = x;
        this.m_Y = y;
    }

    public Coor(Coor coor) {
        this.m_X = coor.m_X;
        this.m_Y = coor.m_Y;
    }
}
