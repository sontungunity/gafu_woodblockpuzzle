using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerInput
{
    public Transform I_Transform { get; }
    public bool CanSelect { get; }
    public bool HandleDown(Vector2 positionWorld);
    public void HandleMove(Vector2 positionWorld);
    public void HandleUp(Vector2 positionWorld);
}

public enum StatusMove {
    NON_MOVE,
    MOVE_TO,
}
