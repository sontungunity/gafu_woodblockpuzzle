using STU;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameManager : Singleton<InGameManager>
{
    [SerializeField] private Camera m_Camera;
    [SerializeField] private Board board;
    [SerializeField] private BlockSpawnerManager spawner;
    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private AudioClip m_musicInGame;
    public Camera MainCamera => m_Camera;
    public Board Board => board;
    public BlockSpawnerManager SpawnerManager => spawner;
    public PlayerInput PlayerInput => playerInput;
    private PlayerData playerData => DataManager.Instance.PlayerData;
    private GameStatus statusGame;
    public GameStatus Status { 
        get { return statusGame; }
        set {
            if(statusGame!=value) {
                statusGame = value;
                EventDispatcher.Dispatch<EventKey.EventGameStatus>(new EventKey.EventGameStatus(statusGame));
            }
        }
    }

    public int m_Score;
    public bool moreChance;
    public bool highScore;

    private void Start() {
        Status = GameStatus.NORMAL;
        board.Show(playerData.TurnPlaySave.boardSave);
        spawner.Show(playerData.TurnPlaySave.LstBlockSave);
        m_Score = playerData.TurnPlaySave.score;
        moreChance = playerData.TurnPlaySave.moreChance;
        highScore = playerData.TurnPlaySave.highScore;
        SoundManager.Instance.PlayMusic(m_musicInGame);
        EventDispatcher.Dispatch<EventKey.ScoreChange>(new EventKey.ScoreChange());
        if(playerData.Tutorial==false) {
            TutorialManager.Instance.ShowTutorial(TutorialID.INGAME);
        }
    }

    public void OneMoreChance() {
        board.SetStatusCell(Cell.Status.NORMAL);
        StartCoroutine(board.BreakGrid(new List<int>() { 3, 4, 5, 6 }, new List<int>() { 3, 4, 5, 6 }, () => {
            moreChance = true;
            Status = GameStatus.NORMAL;
        }));
    }

    public void Replay() {
        Status = GameStatus.NORMAL;
        board.Show();
        spawner.Show();
        m_Score = 0;
        moreChance = false;
        highScore = false;
        SoundManager.Instance.PlayMusic(m_musicInGame);
        EventDispatcher.Dispatch<EventKey.ScoreChange>(new EventKey.ScoreChange());
    }

    [ContextMenu("Lose")]
    public void Lose() {
        Status = GameStatus.LOSE;
        board.DoHandleLose(() => {
            if(moreChance) {
                FrameManager.Instance.Push<ResultFrame>().SetHighScore(highScore);
            } else {
                FrameManager.Instance.Push<OneMoreChance>();
            }
        });
    }

    public void AddScore(int score) {
        if(score > 0) {
            this.m_Score += score;
            EventDispatcher.Dispatch<EventKey.ScoreChange>(new EventKey.ScoreChange());
            if(this.m_Score > playerData.HighScore) {
                playerData.HighScore = this.m_Score;
                EventDispatcher.Dispatch<EventKey.HightScore>(new EventKey.HightScore());
                if(!highScore) {
                    highScore = true;
                    FrameManager.Instance.Push<HighscoreFrame>();
                }
            }
        }
    }

    public void SaveTurnPlay() {
        if(Status == GameStatus.LOSE) {
            playerData.TurnPlaySave = new TurnPlaySave();
            return;
        }
        if(BooterManager.Instance.IsDoing) {
            BooterManager.Instance.TurnOff();
        }
        playerData.TurnPlaySave.score = m_Score;
        playerData.TurnPlaySave.moreChance = moreChance;
        playerData.TurnPlaySave.highScore = highScore;
        playerData.TurnPlaySave.boardSave = board.GetBoardSave();
        playerData.TurnPlaySave.LstBlockSave = spawner.GetListBlockSave();
    }



    public enum GameStatus {
        NORMAL,
        PAUSE,
        LOSE,
    }
}
