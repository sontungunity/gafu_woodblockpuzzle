using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {
    [SerializeField] private Transform root_LstMaterials;
    [SerializeField] private GameObject m_objLight;
    [SerializeField] private ParticleSystem m_effect;
    public Coor Coor;
    public List<MaterialBase> LstMaterial;
    public bool HasMaterial => LstMaterial.Count > 0;
    private Tween tween;
    public void Add(params MaterialID[] lstMaterialID) {
        foreach(var ID in lstMaterialID) {
            MaterialBase prefMaterial = DataManager.Instance.GetMaterialByID(ID);
            MaterialBase material = prefMaterial.Spawn();
            Add(material);
        }
    }

    public void Add(params MaterialBase[] lstMaterial) {
        foreach(var material in lstMaterial) {
            material.transform.parent = root_LstMaterials;
            material.transform.localPosition = Vector3.zero;
            material.transform.localScale = Vector3.one;
            material.transform.localEulerAngles = Vector3.one;
            material.SetOrderLayer(LstMaterial.Count * 5);
            material.Show(MaterialBase.StatusMaterial.NORMAL);
            LstMaterial.Add(material);
        }
    }

    public void GetFromOrtherCell(Cell cell) {
        Add(cell.LstMaterial.ToArray());
        cell.LstMaterial.Clear();
    }

    public void ClearMaterial(bool getMaterial = true) {
        foreach(var material in LstMaterial) {
            material.ClearMaterial(getMaterial);
        }
        LstMaterial.Clear();
    }

    public void Disable(bool v) {
        foreach(var material in LstMaterial) {
            material.Show(v ? MaterialBase.StatusMaterial.DISABLE : MaterialBase.StatusMaterial.NORMAL);
        }
    }

    public void SetUpDefaul() {
        m_objLight.gameObject.SetActive(false);
        foreach(var materail in LstMaterial) {
            materail.Recycle();
        }
        LstMaterial.Clear();
        transform.eulerAngles = Vector3.zero;
        SetStatus(Status.NORMAL);
    }

    public void SetStatus(Status status) {
        switch(status) {
            case Status.NORMAL:
                foreach(var mar in LstMaterial) {
                    mar.Show(MaterialBase.StatusMaterial.NORMAL);
                }
                m_objLight.SetActive(false);
                return;
            case Status.LIGHT:
                foreach(var mar in LstMaterial) {
                    mar.Show(MaterialBase.StatusMaterial.NORMAL);
                }
                m_objLight.SetActive(true);
                return;
            case Status.DARK:
                foreach(var mar in LstMaterial) {
                    mar.Show(MaterialBase.StatusMaterial.DISABLE);
                }
                m_objLight.SetActive(false);
                return;
        }
    }

    public void Scale(Vector3 scaleTarget,float time) {
        tween.CheckKillTween();
        tween = transform.DOScale(scaleTarget, time);
    }

    public enum Status {
        NORMAL,
        LIGHT,
        DARK
    }
}
