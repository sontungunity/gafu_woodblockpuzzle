using STU;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    [SerializeField] private Camera m_MainCamera;
    [Header("MoveBlock")]
    [SerializeField] private float speed;
    private IPlayerInput i_PlayerInput;
    private InGameManager ingameManager => InGameManager.Instance;
    private Board board => InGameManager.Instance.Board;

    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.EventGameStatus>(HanldGameChangeStatus);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.EventGameStatus>(HanldGameChangeStatus);
    }

    private void HanldGameChangeStatus(EventKey.EventGameStatus evt) {
        if((evt.Status == InGameManager.GameStatus.LOSE || evt.Status == InGameManager.GameStatus.PAUSE) && i_PlayerInput != null) {
            HandleRelease(Vector2.zero);
        }
    }



    private void Update() {
        if(ingameManager.Status != InGameManager.GameStatus.NORMAL)
            return;

#if UNITY_EDITOR
        Vector2 mousePos = Input.mousePosition;
        if(Input.GetMouseButtonDown(0)) {
            HandlePressed(mousePos);
        }

        if(Input.GetMouseButton(0)) {
            HandleMove(mousePos);
        }

        if(Input.GetMouseButtonUp(0)) {
            HandleRelease(mousePos);
        }

        if(Input.GetKeyDown(KeyCode.Space)) {
            //GameManager.Instance.pBoosterController.CollectStars(new List<Vector3>() { Vector3.zero });
            DataManager.Instance.PlayerData.AddItem(new ItemStack(ItemID.STAR, 1));
            Vector2 start = Camera.main.WorldToScreenPoint(Vector2.zero);
            EventDispatcher.Dispatch<EventKey.GetItemFromUI>(new EventKey.GetItemFromUI(new ItemStack(ItemID.STAR, 1), start));
        }
#elif UNITY_ANDROID || UNITY_IOS
        if(Input.touchCount == 1) {
            var touch = Input.GetTouch(0);
            Vector2 touchPos = touch.position;
            if(touch.phase == TouchPhase.Began) {
                HandlePressed(touchPos);
            }

            if(touch.phase == TouchPhase.Moved) {
                HandleMove(touchPos);
            }
            if(touch.phase == TouchPhase.Ended) {
                HandleRelease(touchPos);
            }
        }
#endif
    }

    private void HandlePressed(Vector2 touchPos) {
        Vector2 worldPos = m_MainCamera.ScreenToWorldPoint(touchPos);
        RaycastHit2D hit = Physics2D.Raycast(worldPos, Vector2.zero);
        if(hit.collider != null) {
            var iPlayerInput = hit.collider.GetComponent<IPlayerInput>();
            if(iPlayerInput != null && iPlayerInput.CanSelect) {
                EventDispatcher.Dispatch<EventKey.EventInputSelect>(new EventKey.EventInputSelect(true, iPlayerInput,worldPos));
                this.i_PlayerInput = iPlayerInput;
                this.i_PlayerInput.HandleDown(worldPos);
            }
        }
    }

    private void HandleMove(Vector2 touchPos) {
        if(i_PlayerInput != null) {
            Vector2 worldPos = m_MainCamera.ScreenToWorldPoint(touchPos);
            //float speedDeltal = speed * Time.deltaTime;
            i_PlayerInput.HandleMove(worldPos); //
        }
    }

    private void HandleRelease(Vector2 touchPos) {
        if(i_PlayerInput != null) {
            Vector2 worldPos = m_MainCamera.ScreenToWorldPoint(touchPos);
            EventDispatcher.Dispatch<EventKey.EventInputSelect>(new EventKey.EventInputSelect(false, i_PlayerInput, worldPos));
            i_PlayerInput.HandleUp(worldPos);
            i_PlayerInput = null;
        }
    }
}
