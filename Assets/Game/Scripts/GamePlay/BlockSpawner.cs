using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
public class BlockSpawner : MonoBehaviour, IPlayerInput {
    [SerializeField] private Block block;
    [SerializeField] private AudioClip sound_Select,sound_DropFail,sound_DropBoard;
    [SerializeField] private Vector2 deltaMove;
    [SerializeField] private float speed,timeTarget;
    private Board m_Board => InGameManager.Instance.Board;
    public Block Block => block;

    public bool CanSelect => block.HasCell;

    public Transform I_Transform => transform;
    private Vector3 startPosition;
    private Vector3 targetPosition;
    private StatusMove sttMove = StatusMove.NON_MOVE;
    private Tween tween;
    public bool HandleDown(Vector2 positionWorld) {
        if(!block.HasCell) {
            return false;
        }
        SoundManager.Instance.PlaySound(sound_Select);
        this.block.OnSelect(true);
        startPosition = positionWorld;
        targetPosition = positionWorld + deltaMove;
        sttMove = StatusMove.MOVE_TO;
        tween.CheckKillTween();
        tween = block.transform.DOMove(targetPosition, timeTarget).SetEase(Ease.Linear).OnComplete(()=> {
            tween = null;
        });
        return true;
    }
    private List<Cell> lstCell_Plance = new List<Cell>();
    private List<int> lstCol_Break = new List<int>();
    private List<int> lstRow_Break = new List<int>();
    public List<Cell> LstCell_Plance => lstCell_Plance;
    public void HandleMove(Vector2 positionWorld) {
        if(!block.HasCell) {
            return;
        }
        if(tween!= null) {
            float distan = Vector3.Distance(startPosition,positionWorld);
            if(distan > 0.2f) {
                tween.CheckKillTween();
                tween = null;
            }
        }
        targetPosition = positionWorld + deltaMove;
    }

    List<Cell> noDupes = new List<Cell>();
    public void HandleUp(Vector2 positionWorld) {
        sttMove = StatusMove.NON_MOVE;
        TurnOnLigt(false);
        noDupes.Clear();
        noDupes = block.LstCell.Distinct().ToList();
        bool release = false;
        if(noDupes.Count() == lstCell_Plance.Count()) {
            SoundManager.Instance.PlaySound(sound_DropBoard);
            InGameManager.Instance.Board.PlanceBlock(block, lstCell_Plance, lstCol_Break, lstRow_Break);
            release = true;
        }

        if(release) {
            block.transform.localPosition = Vector3.zero;
        } else {
            float distan = Vector3.Distance(block.transform.localPosition,Vector3.zero);
            float time = distan/speed;
            tween.CheckKillTween();
            tween = block.transform.DOLocalMove(Vector3.zero, time).SetEase(Ease.Linear).OnComplete(() => {
                block.OnSelect(false);
                SoundManager.Instance.PlaySound(sound_DropFail);
                tween = null;
            });
        }
    }

    private void TurnOnLigt(bool turnOn) {
        m_Board.TurnOnLight(lstCell_Plance, turnOn);

        foreach(var index in lstCol_Break) {
            m_Board.TurnOnLight(m_Board.GetGridElementByAxits(index, Board.Axis.COL), turnOn);
        }

        foreach(var index in lstRow_Break) {
            m_Board.TurnOnLight(m_Board.GetGridElementByAxits(index, Board.Axis.ROW), turnOn);
        }
    }

    private void LateUpdate() {
        if(sttMove == StatusMove.NON_MOVE) {
            return;
        }

        if(sttMove == StatusMove.MOVE_TO) {
            //float cur_speed = 0;
            //if(Vector3.Distance(block.transform.position, targetPosition) >= 6) {
            //    timeMove += Time.deltaTime * 2;
            //} else {
            //    timeMove += Time.deltaTime;
            //}
            //cur_speed = Mathf.Min(timeMove / timeMaxSpeed * speed, speed);
            if(tween == null) {
                float distance = Time.deltaTime * speed;
                block.transform.position = Vector3.MoveTowards(block.transform.position, targetPosition, distance);
                TurnOnLigt(false);
                m_Board.SetupForBlock(block, lstCell_Plance, lstCol_Break, lstRow_Break);
                TurnOnLigt(true);
            }
        } 
        //else if(sttMove == StatusMove.MOVE_BACK) {
        //    float distance = Time.deltaTime * speed;
        //    block.transform.position = Vector3.MoveTowards(block.transform.position, targetPosition, distance);
        //    if(Vector3.Distance(block.transform.position, targetPosition) <0.1f) {
        //        sttMove = StatusMove.NON_MOVE;
        //        block.transform.localPosition = Vector3.zero;
        //        block.OnSelect(false);
        //        SoundManager.Instance.PlaySound(sound_DropFail);
        //    }
        //}
    }
}
