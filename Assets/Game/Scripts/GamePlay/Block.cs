using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using DG.Tweening;
using System.Linq;
using STU;
//using STU;

public class Block : MonoBehaviour {
    // Property Select
    private static float SELECT_SCALE_BLOCK = 1f;
    private static float SELECT_SCALE_ELEMENT = 0.95f;
    // Property DisSelect
    private static float UNSELECT_SCALE_BLOCK = 0.62f;
    private static float UNSELECT_SCALE_ELEMENT = 1f;
    [SerializeField]private Cell cellPref;
    [SerializeField]private SortingGroup sortingGroup;
    [SerializeField]private float timeScale;
    public List<Cell> LstCell = new List<Cell>();
    //Sofe data forBlock
    //public BlockData data;
    //public BlockAngle blockAngle;
    int optionSpace = 0;
    List<Coor> lstSpace = new List<Coor>();
    public bool HasCell {
        get {
            if(LstCell!=null && LstCell.Count>0) {
                foreach(var cell in LstCell) {
                    if(cell.HasMaterial) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    public int OptionSpace => optionSpace;
    private Tween tween;
    private Tween tweenRotation;
    public void Show(BlockSave blockSave) {
        SetUpDefaul();
        if(blockSave == null) {
            for(int y = LstCell.Count - 1; y >= 0; y--) {
                LstCell[y].Recycle();
                LstCell.RemoveAt(y);
            }
            return;
        }
        Vector2 center = blockSave.GetCenter();
        int i = 0;
        for(i = 0; i < blockSave.lstCell.Count; i++) {
            if(i >= LstCell.Count) {
                LstCell.Add(cellPref.Spawn(transform));
            }
            LstCell[i].SetUpDefaul();
            LstCell[i].Coor = new Coor(blockSave.lstCell[i].coor.m_X, blockSave.lstCell[i].coor.m_Y);
            LstCell[i].Add(blockSave.lstCell[i].LstMaterialID.ToArray());
            LstCell[i].transform.localPosition = new Vector2(blockSave.lstCell[i].coor.m_X - center.x, blockSave.lstCell[i].coor.m_Y - center.y);
        }

        for(int y = LstCell.Count - 1; y >= i; y--) {
            LstCell[y].Recycle();
            LstCell.RemoveAt(y);
        }
        GetPlance();
        OnSelect(false);
    }

    private void SetUpCoor() {
        BlockAngle blockAngle = transform.eulerAngles.z.GetAngleToFloat();
        for(int i = 0; i < LstCell.Count; i++) {
            Vector2 indexVector = LstCell[i].transform.localPosition - LstCell[0].transform.localPosition;
            LstCell[i].Coor = indexVector.GetCoorFromVectorAngle(blockAngle);
            LstCell[i].transform.eulerAngles = Vector3.zero;
        }
    }

    public void SetUpDefaul() {
        transform.localPosition = Vector3.zero;
        transform.eulerAngles = Vector3.zero;
        OnSelect(false);
        foreach(var cell in LstCell) {
            cell.Recycle();
        }
        LstCell.Clear();
    }

    public void Disable(bool v) {
        foreach(var element in LstCell) {
            element.Disable(v);
        }
    }

    public void OnSelect(bool Select) {
        sortingGroup.enabled = Select;
        if(Select) {
            tween.CheckKillTween();
            tween = transform.DOScale( Vector3.one * SELECT_SCALE_BLOCK, timeScale);
            foreach(var cell in LstCell) {
                cell.Scale(Vector3.one * SELECT_SCALE_ELEMENT, timeScale);
            }
        } else {
            tween.CheckKillTween();
            tween = transform.DOScale(Vector3.one * UNSELECT_SCALE_BLOCK, timeScale);
            foreach(var cell in LstCell) {
                cell.Scale(Vector3.one * UNSELECT_SCALE_ELEMENT, timeScale);
            }
        }
    }

    public void AddMaterial(MaterialID material, int cur_int) {
        if(!HasCell) {
            return;
        }
        int amout = LstCell.Count();
        int[] arryIndex = new int[amout];
        for(int i = 0; i < amout; i++) {
            arryIndex[i] = i;
        }
        arryIndex.Shuffle();

        for(int y = 0; y < cur_int; y++) {
            LstCell.ElementAt(arryIndex[y]).Add(material);
        }
    }

    public List<Coor> GetIndexBlock() {
        List<Coor> result = new List<Coor>();
        for(int i = 0; i < LstCell.Count; i++) {
            if(LstCell[i].HasMaterial) {
                result.Add(LstCell[i].Coor);
            }
        }
        return result;
    }

    public void Rotation(float angle) {
        if(tweenRotation != null) {
            return;
        }
        float newAngle = transform.eulerAngles.z + angle;
        tweenRotation = transform.DORotate(new Vector3(0, 0, newAngle), 0.2f).OnComplete(() => {
            SetUpCoor();
            GetPlance();
            tweenRotation = null;
        });
    }

    [ContextMenu("GetPlance")]
    public void GetPlance() {
        lstSpace.Clear();
        optionSpace = 0;
        bool canPlance = InGameManager.Instance.Board.CheckSpace(GetIndexBlock(),out lstSpace,out optionSpace);
        Disable(!canPlance);
    }

    public BlockSave GetBlockSave() {
        BlockSave result = new BlockSave();
        foreach(var cell in LstCell) {
            CellSave cells = new CellSave();
            cells.coor = cell.Coor;
            foreach(var mar in cell.LstMaterial) {
                cells.LstMaterialID.Add(mar.ID);
            }
            result.lstCell.Add(cells);
        }
        return result;
    }

    private void OnDisable() {
        tween.CheckKillTween();
        tweenRotation.CheckKillTween();
    }

    private void OnDestroy() {
        tween.CheckKillTween();
        tweenRotation.CheckKillTween();
    }
}
