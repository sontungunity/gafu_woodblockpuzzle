﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using STU;
using DG.Tweening;

public class BlockSpawnerManager : MonoBehaviour {
    [SerializeField] private List<BlockSpawner> lstBlockSpawner;
    [SerializeField] private AudioClip audioSwap;
    private DataManager data => DataManager.Instance;
    public List<BlockSpawner> LstBlockSpawner => lstBlockSpawner;
    private Tween tween; 
    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.BoardChange>(HandlerBoardChange);
        EventDispatcher.AddListener<EventKey.BlockChange>(HandlerBlockChange);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.BoardChange>(HandlerBoardChange);
        EventDispatcher.RemoveListener<EventKey.BlockChange>(HandlerBlockChange);
        tween.CheckKillTween();
    }

    private void OnDestroy() {
        tween.CheckKillTween();
    }

    private void HandlerBoardChange(EventKey.BoardChange evt) {
        //Set up all block
        foreach(var blockSpawner in lstBlockSpawner) {
            if(blockSpawner.Block.HasCell) {
                blockSpawner.Block.GetPlance();
            }
        }
        HandlerBlockChange();
    }

    private void HandlerBlockChange(EventKey.BlockChange evt) {
        HandlerBlockChange();
    }

    private void HandlerBlockChange() {
        if(BooterManager.Instance.IsDoing) {
            return;
        }

        int amoutBlockActive = 0;
        int amountOption = 0;
        foreach(var spawner in LstBlockSpawner) {
            if(spawner.Block.HasCell) {
                amoutBlockActive++;
                amountOption += spawner.Block.OptionSpace;
            }
        }

        // hêt khối tạo khối mới
        if(amoutBlockActive == 0) {
            GetNewBlocks();
            return;
        }
        // Có nhiều lựa chọn đặt khối
        if(amountOption != 0) {
            return;
        }
        InGameManager.Instance.Lose();
    }

    public void Show(List<BlockSave> lstBlockSave = null) {
        for(int i = 0; i < lstBlockSpawner.Count; i++) {
            lstBlockSpawner[i].Block.Show(lstBlockSave.GetElementCheckNull(i));
        }
        HandlerBlockChange();
    }

    public void GetNewBlocks(bool delay = false) {
        tween.CheckKillTween();
        foreach(var blockSpawner in lstBlockSpawner) {
            if(blockSpawner.Block.HasCell) {
                foreach(var cell in blockSpawner.Block.LstCell) {
                    cell.ClearMaterial(false);
                }
            }
        }

        if(delay) {
            SoundManager.Instance.PlaySound(audioSwap);
            tween = DOVirtual.DelayedCall(1.5f, () => {
                LogicSpawnerBlock();
                if(InGameManager.Instance.Board.GetCombo3) {
                    AddStar();
                    InGameManager.Instance.Board.GetCombo3 = false;
                }
                tween = null;
            });
        } else {
            LogicSpawnerBlock();
            if(InGameManager.Instance.Board.GetCombo3) {//InGameManager.Instance.Board.GetCombo3
                AddStar();
                InGameManager.Instance.Board.GetCombo3 = false;
            }
            tween = null;
        }

    }

    private void AddStar() {
        int randomStart = Random.Range(1,2);
        for(int i = 0; i < lstBlockSpawner.Count; i++) {
            if(randomStart <= 0) {
                return;
            }

            int cur_int = 0;
            if(i == lstBlockSpawner.Count - 1) {
                cur_int = randomStart;
            } else {
                cur_int = Random.Range(0, randomStart);
            }

            if(cur_int > 2) {
                cur_int = 2;
            }

            cur_int = Mathf.Min(cur_int, lstBlockSpawner[i].Block.LstCell.Count());

            lstBlockSpawner[i].Block.AddMaterial(MaterialID.STAR, cur_int);
            randomStart -= cur_int;
        }

    }

    public BlockAngle GetRandomAngle() {
        int index = Random.Range(0,4);
        BlockAngle result = (BlockAngle)index;
        return result;
    }

    List<int> lstIDBlockCanGet = new List<int>();
    Queue<int> queueBlock = new Queue<int>();
    public void LogicSpawnerBlock() {
        //create lst block can get
        lstIDBlockCanGet.Clear();
        foreach(var block in data.LstBlockData) {
            if(!queueBlock.Contains(block.id)) {
                lstIDBlockCanGet.Add(block.id);
            }
        }

        foreach(var spawner in lstBlockSpawner) {
            int indexRandom = Random.Range(0,lstIDBlockCanGet.Count);
            int idBlock = lstIDBlockCanGet[indexRandom];
            BlockData blockdata = data.GetBlockDataByID(idBlock); //
            BlockAngle angle = GetRandomAngle();
            spawner.Block.Show(ConvetBlockDataAngleToBlockSave(blockdata, angle));
            lstIDBlockCanGet.Remove(idBlock);
            queueBlock.Enqueue(idBlock);
        }

        while(queueBlock.Count > 6) {
            queueBlock.Dequeue();
        }
        HandlerBlockChange();
    }

    public BlockSave ConvetBlockDataAngleToBlockSave(BlockData data, BlockAngle angle) {
        BlockSave result = new BlockSave();
        foreach(var pos in data.lstBlockPos) {
            CellSave cellS = new CellSave();
            cellS.coor = pos.GetCoorFromVectorAngle(angle);
            cellS.LstMaterialID.Add(MaterialID.BLOCKNORMAL);
            result.lstCell.Add(cellS);
        }
        return result;
    }

    public List<BlockSave> GetListBlockSave() {
        List<BlockSave> result = new List<BlockSave>();
        foreach(var blockSpawner in lstBlockSpawner) {
            result.Add(blockSpawner.Block.GetBlockSave());
        }
        return result;
    }
}
