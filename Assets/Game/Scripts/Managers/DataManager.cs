using STU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class DataManager : Singleton<DataManager>
{
    public string FILE_DATA_PATH => Path.Combine(Application.persistentDataPath, "SaveData");
    public string FILE_DATA_NAME => "GameData.dat";
    private string pathFlie => Path.Combine(FILE_DATA_PATH, FILE_DATA_NAME);
    private const string BLOCKDATA_PATH = "BLockDatas";
    private const string MATERIAL_PATH = "Materials";
    private const string ITEMDATA_PATH = "ItemDatas";
    [SerializeField] private List<BlockData> lstBlockData = new List<BlockData>();
    [SerializeField] private List<MaterialBase> lstMaterial = new List<MaterialBase>();
    [SerializeField] private List<ItemData> lstItemData = new List<ItemData>();
    public IEnumerable<BlockData> LstBlockData => lstBlockData;
    private PlayerData playerData;
    public PlayerData PlayerData {
        get {
            if(playerData != null) {
                return playerData;
            } else {
                LoadData();
                if(playerData == null) {
                    playerData = new PlayerData();
                }
                return playerData;
            }
        }
    }
    protected override void OnAwake() {
        base.OnAwake();
        LoadData();
        LoadBlockData();
        LoadMaterial();
        LoadItemData();
    }

    public ItemData GetItemDataByID(ItemID itemID) {
        ItemData result = lstItemData.Find(x => x.ItemID == itemID);
        return result;
    }

    public MaterialBase GetMaterialByID(MaterialID materialID) {
        MaterialBase result = lstMaterial.Find(x=>x.ID == materialID);
        return result;
    }

    public BlockData GetBlockDataByID(int id) {
        BlockData result = lstBlockData.Find(x=>x.id == id);
        return result;
    }

    #region Save and Load Data
    public void SaveData() {
        //PlayerPrefs.SetString(PLAYER_KEY, JsonUtility.ToJson(playerData));
        bool sceneInGame = string.Compare(SceneManager.SCENE_GAME, UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) == 0;
        if(sceneInGame && InGameManager.Instance!=null) {
            InGameManager.Instance.SaveTurnPlay();
        }
        string data = JsonUtility.ToJson(playerData);
        if(!Directory.Exists(FILE_DATA_PATH)) {
            Debug.LogError("CreateFile");
            Directory.CreateDirectory(FILE_DATA_PATH);
        }

        try {
            using(StreamWriter writer = File.CreateText(pathFlie)) {
                writer.Write(data);
                Debug.Log($"[DATA] Write file completed.\n <path>: {pathFlie}\n <content>: {data}");
                writer.Close();
            }
        } catch(Exception e) {
            Debug.LogError($"[DATA] Write file failed.\n <path>: {pathFlie}\n <error>: {e}");
        }
    }
    public void LoadData() {
        if(File.Exists(pathFlie)) {
            try {
                using(StreamReader reader = File.OpenText(pathFlie)) {
                    string data = reader.ReadToEnd();
                    playerData = JsonUtility.FromJson<PlayerData>(data);
                    reader.Close();
                }
            } catch(Exception e) {
                Debug.Log($"[DATA] Read file no found.\n <path>: {pathFlie}\n <error>: {e}");
            }

        } else {
            Debug.Log($"[DATA] Read file no found.\n <path>: {pathFlie}");
            playerData = new PlayerData();
        }
        //SaveData();
    }
    private void LoadBlockData() {
        foreach(BlockData block in Resources.LoadAll<BlockData>(BLOCKDATA_PATH)) {
            lstBlockData.Add(block);
        }
    }

    private void LoadMaterial() {
        foreach(MaterialBase material in Resources.LoadAll<MaterialBase>(MATERIAL_PATH)) {
            lstMaterial.Add(material);
        }
    }

    private void LoadItemData() {
        foreach(ItemData item in Resources.LoadAll<ItemData>(ITEMDATA_PATH)) {
            lstItemData.Add(item);
        }
    }
    #endregion

    [ContextMenu("SetUpIndexBlock")]
    private void SetUpIndexBlock() {
        int i = 0;
        foreach(var block in LstBlockData) {
            block.id = i;
            i++;
        }
    }
}
