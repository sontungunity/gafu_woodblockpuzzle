
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Extentions {
    public static T GetElementCheckNull<T>(this List<T> lstOject, int index) {
        if(lstOject != null && index < lstOject.Count && index >= 0) {
            return lstOject[index];
        }
        return default(T);
    }
    public static void CheckKillTween(this Tween tween, bool OnComplate = false) {
        if(tween != null) {
            tween.Kill(OnComplate);
        }
    }

    public static float GetFloatAngleByBlockAngle(this BlockAngle angle) {
        switch(angle) {
            case BlockAngle.ANGLE0:
                return 0f;
            case BlockAngle.ANGLE_90:
                return 90f;
            case BlockAngle.ANGLE_180:
                return 180f;
            case BlockAngle.ANGLE_270:
                return 270f;
            default:
                return 0f;
        }
    }

    public static T[] GetRow<T>(this T[,] matrix, int columnNumber) {
        return Enumerable.Range(0, matrix.GetLength(0))
                .Select(x => matrix[x, columnNumber])
                .ToArray();
    }

    public static T[] GetColumn<T>(this T[,] matrix, int rowNumber) {
        return Enumerable.Range(0, matrix.GetLength(1))
                .Select(x => matrix[rowNumber, x])
                .ToArray();
    }

    public static ItemData GetDataByID(this ItemID id) {
        return DataManager.Instance.GetItemDataByID(id);
    }

    public static ItemStack GetSaveByID(this ItemID id) {
        return DataManager.Instance.PlayerData.GetItemSaveByItemId(id);
    }

    public static void Shuffle<T>(this T[] decklist) {
        T tempGO = default;
        for(int i = 0; i < decklist.Length; i++) {
            int rnd = Random.Range(0, decklist.Length);
            tempGO = decklist[rnd];
            decklist[rnd] = decklist[i];
            decklist[i] = tempGO;
        }
    }

    public static Coor GetCoorFromVectorAngle(this Vector2 vt,BlockAngle angle) {
        switch(angle) {
            case BlockAngle.ANGLE0:
                return new Coor(Mathf.RoundToInt(vt.x), Mathf.RoundToInt(vt.y));
            case BlockAngle.ANGLE_90:
                return new Coor(Mathf.RoundToInt(-vt.y), Mathf.RoundToInt(vt.x));
            case BlockAngle.ANGLE_180:
                return new Coor(Mathf.RoundToInt(-vt.x), Mathf.RoundToInt(-vt.y));
            case BlockAngle.ANGLE_270:
                return new Coor(Mathf.RoundToInt(vt.y), Mathf.RoundToInt(-vt.x));
            default:
                return new Coor(Mathf.RoundToInt(vt.x), Mathf.RoundToInt(vt.y));
        }
    }

    public static BlockAngle GetAngleToFloat(this float angle) {
        float anglePositive = angle%360;
        if(anglePositive == 0) {
            return BlockAngle.ANGLE0;
        } else if(anglePositive == 90) {
            return BlockAngle.ANGLE_90;
        } else if(anglePositive == 180) {
            return BlockAngle.ANGLE_180;
        } else if(anglePositive == 270) {
            return BlockAngle.ANGLE_270;
        } else {
            Dictionary<BlockAngle,float> direcSp =  new Dictionary<BlockAngle, float>();
            BlockAngle[] arrayAngle = new BlockAngle[]{BlockAngle.ANGLE0,BlockAngle.ANGLE_90,BlockAngle.ANGLE_180,BlockAngle.ANGLE_270};
            Debug.LogError("Angle is wrong!!");
            direcSp.Clear();
            for(int i = 0; i < arrayAngle.Count(); i++) {
                float nearDis = Mathf.Abs(anglePositive-arrayAngle[i].GetFloatAngleByBlockAngle());
                direcSp.Add(arrayAngle[i], nearDis);
            }
            var order =  direcSp.OrderBy(value => value.Value);
            var a = direcSp.ElementAt(0);
            foreach(var kv in direcSp) {
                if(kv.Value < a.Value) {
                    a = kv;
                }
            }
            return a.Key;
        }
    }
}

public enum ItemID {
    NONE = 0,
    REMOVEADS = 1,
    UNLOCKSKINS = 2,
    STAR = 3,

    #region Booter 10->
    Booter_Swap = 10,
    Booter_Rotation = 11,
    Booter_Bomb = 12
    #endregion
}

public enum MaterialID {
    NONE = 0,
    BLOCKNORMAL = 1,
    STAR =2,
}
