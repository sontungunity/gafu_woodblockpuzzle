using STU;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionController : Singleton<CollectionController> {
    [SerializeField] private CollectionItem m_colItemPref;

    public void GetItemStack(ItemStack itemStack,Vector2 startPosition,Vector2 endPosition, Action callback ) {
        int amount = itemStack.Amount;
        if(amount == 1) {
            CollectionItem colItem = m_colItemPref.Spawn();
            colItem.GetComponent<RectTransform>().SetParent(transform);
            colItem.Show(itemStack.ItemID.GetDataByID().Icon);
            colItem.Move(startPosition,endPosition,callback);
            return;
        } 
    }
}
