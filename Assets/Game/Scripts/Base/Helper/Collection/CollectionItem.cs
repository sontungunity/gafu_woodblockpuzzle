using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItem : MonoBehaviour {
    [SerializeField] private Image icon;
    [SerializeField] private float speed;
    [SerializeField] private AudioClip m_sound;
    public void Show(Sprite icon) {
        this.icon.sprite = icon;
    }

    public void Move(Vector2 startPos, Vector2 endPos, Action callback = null) {
        transform.position = startPos;
        float time = Vector2.Distance(startPos,endPos)/speed;
        transform.DOMove(endPos, time).SetEase(Ease.Linear).OnComplete(()=> {
            SoundManager.Instance.PlaySound(m_sound);
            callback.Invoke();
            gameObject.Recycle();
        });
    }
}
