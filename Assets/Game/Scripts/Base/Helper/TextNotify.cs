using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using STU;
using System;
using Spine.Unity;
using Spine;

public class TextNotify : Singleton<TextNotify> {
    public static string ADS_FAIL = "Ads appear error";
    [SerializeField] private TextMeshProUGUI txt_Notify;
    [SerializeField] private float hightMore;
    [SerializeField] private float timeDisapear;
    [SerializeField] private SkeletonGraphic anim;
    Tween tween;
    private Action evtComplate;
    private void Awake() {
        anim.AnimationState.Complete += HandleEventComplete;
    }

    private void HandleEventComplete(TrackEntry trackEntry) {
        evtComplate?.Invoke();
        evtComplate = null;
    }

    private void Start() {
        txt_Notify.gameObject.SetActive(false);
        anim.gameObject.SetActive(false);
    }

    public void Show(string text) {
        txt_Notify.transform.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        txt_Notify.gameObject.SetActive(true);
        txt_Notify.text = text;
        txt_Notify.transform.localScale = Vector3.one;
        if(tween != null && tween.IsPlaying() == true) {
            tween.Kill();
        }
        tween = txt_Notify.transform.DOLocalMoveY(hightMore, timeDisapear).OnComplete(() => {
            txt_Notify.gameObject.SetActive(false);
        });
    }

    [ContextMenu("ShowDemo")]
    public void ShowDemo() {
        Show("Show Demo");
    }

    [ContextMenu("SetUpTextNotify")]
    public void SetUpTextNotify() {
        gameObject.name = "TextNotify";
        hightMore = 200f;
        timeDisapear = 1f;
        var canvas = gameObject.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.sortingLayerName = "UI";
        canvas.sortingOrder = 100;
        GameObject obj = new GameObject("Txt_Move", typeof(RectTransform));
        obj.GetComponent<RectTransform>().SetParent(transform);
        var rect = obj.GetComponent<RectTransform>();
        rect.anchorMin = new Vector2(0.5f, 0.5f);
        rect.anchorMax = new Vector2(0.5f, 0.5f);
        rect.transform.localScale = Vector3.one;
        rect.sizeDelta = new Vector2(700, 150);
        var textMeshPro = obj.AddComponent<TextMeshProUGUI>();
        textMeshPro.enableAutoSizing = true;
    }


    public void ShowNoMoreMoves(Action callback = null) {
        anim.gameObject.SetActive(true);
        this.evtComplate = () => {
            anim.gameObject.SetActive(false);
            callback?.Invoke();
        };
        Skin skin = new Skin("Skin");
        skin.AddSkin(anim.Skeleton.Data.FindSkin("Nomoremove"));
        anim.Skeleton.SetSkin(skin);
        anim.AnimationState.SetAnimation(0, "Animations", false);
    }

    public void ShowTextByCombo(int combo, Action callback = null) {
        if(combo == 1) {
            return;
        }

        anim.gameObject.SetActive(true);
        this.evtComplate = () => {
            anim.gameObject.SetActive(false);
            callback?.Invoke();
        };
        if(combo == 2) {
            Skin skin = new Skin("Skin");
            int indexRandom = UnityEngine.Random.Range(0,2);
            skin.AddSkin(anim.Skeleton.Data.FindSkin(indexRandom == 0 ? "Wow" : "Cool"));
            anim.Skeleton.SetSkin(skin);
            anim.AnimationState.SetAnimation(0, "Animations", false);
        } else if(combo == 3) {
            Skin skin = new Skin("Skin");
            skin.AddSkin(anim.Skeleton.Data.FindSkin("Super"));
            anim.Skeleton.SetSkin(skin);
            anim.AnimationState.SetAnimation(0, "Animations", false);
        } else if(combo == 4) {
            Skin skin = new Skin("Skin");
            skin.AddSkin(anim.Skeleton.Data.FindSkin("Perfect"));
            anim.Skeleton.SetSkin(skin);
            anim.AnimationState.SetAnimation(0, "Animations", false);
        } else if(combo == 5) {
            Skin skin = new Skin("Skin");
            skin.AddSkin(anim.Skeleton.Data.FindSkin("Awesome"));
            anim.Skeleton.SetSkin(skin);
            anim.AnimationState.SetAnimation(0, "Animations", false);
        } else if(combo == 6) {
            Skin skin = new Skin("Skin");
            skin.AddSkin(anim.Skeleton.Data.FindSkin("Fantastic"));
            anim.Skeleton.SetSkin(skin);
            anim.AnimationState.SetAnimation(0, "Animations", false);
        }
    }

    [ContextMenu("TestAnimText")]
    public void TestAnimText() {
        ShowNoMoreMoves();
    }
}

