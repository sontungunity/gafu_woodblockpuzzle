﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    [SerializeField] private Button btn_Btn;
    [SerializeField] private AudioClip clip;

    private void Awake() {
        btn_Btn.onClick.AddListener(PlaySound);
    }

    private void PlaySound() {
        SoundManager.Instance.PlaySound(clip);
    }

    private void OnValidate() {
        btn_Btn = GetComponent<Button>();
        if(btn_Btn == null) {
            btn_Btn = gameObject.AddComponent<Button>();
        }
    }

}
