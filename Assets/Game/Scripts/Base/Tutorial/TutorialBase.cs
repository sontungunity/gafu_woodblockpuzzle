using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public abstract class TutorialBase : MonoBehaviour {
    [SerializeField] private TutorialID tutoriaID;
    protected Action callback;
    public TutorialID TutoriaID => tutoriaID;
    public abstract void Show(Action callBack = null);
    protected void SetUIHighlight(GameObject target, bool highlight) {
        Canvas canvas = target.GetComponent<Canvas>();
        GraphicRaycaster graphicRaycaster = target.GetComponent<GraphicRaycaster>();
        if(highlight) {
            if(!canvas)
                canvas = target.AddComponent<Canvas>();
            if(!graphicRaycaster)
                graphicRaycaster = target.AddComponent<GraphicRaycaster>();

            canvas.overrideSorting = true;
            canvas.sortingLayerName = TutorialManager.SORTING_LAYER_NAME;
            canvas.sortingOrder = TutorialManager.SORTING_Order + 1;
        } else {
            if(graphicRaycaster)
                Destroy(graphicRaycaster);
            if(canvas)
                Destroy(canvas);
        }
    }

    protected void SetGameObjectHighlight(GameObject target, bool highlight) {
        SortingGroup sortingGroup = target.GetComponent<SortingGroup>();
        if(highlight) {
            if(!sortingGroup)
                sortingGroup = target.AddComponent<SortingGroup>();

            sortingGroup.sortingLayerName = TutorialManager.SORTING_LAYER_NAME;
            sortingGroup.sortingOrder = TutorialManager.SORTING_Order + 1;
        } else {
            if(sortingGroup) {
                Destroy(sortingGroup);
            }
        }
    }
}

public enum TutorialID {
    NONE,
    START,
    INGAME,
    INGAMEII,
    INGAMEIII,
}
