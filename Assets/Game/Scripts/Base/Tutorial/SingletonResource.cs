using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonResource<T> : MonoBehaviour where T : MonoBehaviour {
    private static T instance;

    public static T Instance {
        get {
            if(instance == null) {
                T prefab = Resources.Load<T>(typeof(T).ToString());
                if(prefab == null) {
                    Debug.LogErrorFormat("[SINGLETON] The prefab {0} could not be found in resource folder!", typeof(T));
                }

                instance = Instantiate(prefab);
            }
            return instance;
        }
    }

    public static bool HasInstance => instance != null;

    private void Awake() {
        if(instance == null) {
            instance = this as T;
            OnAwake();
        } else if(instance != this) {
            Debug.LogWarningFormat("[SINGLETON] There is more than one instance of class {0} in the scene!", typeof(T));
            Destroy(this.gameObject);
        }
    }

    protected virtual void OnAwake() {
    }

    protected virtual void OnDestroy() {
        instance = null;
    }
}
