using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialStart : TutorialBase
{
    [SerializeField] private Vector3 delta;
    [SerializeField] private RectTransform point;
    private Button btnStart;
    public override void Show(Action callBack = null) {
        btnStart = FrameManager.Instance.GetFrame<HomeFrame>().BtnStart;
        point.position = btnStart.transform.GetComponent<RectTransform>().position;
        SetUIHighlight(btnStart.gameObject, true);
        point.GetComponent<Canvas>().sortingLayerName = "Tutorial";
        point.GetComponent<Canvas>().sortingOrder = 10;
        SetUpAnimHand();
    }

    private void SetUpAnimHand() {
        DOTween.Kill("hand_animation_seq");
        Vector3 targetPosition = btnStart.transform.position;
        Vector3 startPos = targetPosition + new Vector3(1,-1);
        point.position = startPos;
        point.localScale = Vector3.one;

        Sequence handMovingSeq = DOTween.Sequence();
        handMovingSeq.Append(point.DOMove(targetPosition, 1f));
        handMovingSeq.Append(point.DOScale(Vector3.one * 0.9f, 0.5f).SetEase(Ease.OutBack));
        handMovingSeq.Append(point.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack));
        handMovingSeq.Append(point.DOMove(startPos, 1f));
        handMovingSeq.SetLoops(-1);
        handMovingSeq.SetId("hand_animation_seq");
    }

    private void OnDisable() {
        DOTween.Kill("hand_animation_seq");
    }

    private void OnDestroy() {
        DOTween.Kill("hand_animation_seq");
    }
}
