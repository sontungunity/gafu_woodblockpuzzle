using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using STU;

public class TutorialInGame : TutorialBase {
    [SerializeField] private BlockSave blockSave;
    [SerializeField] private List<Coor> lstCoor = new List<Coor>();
    [SerializeField] private List<Coor> lstIndexTarget = new List<Coor>();
    [SerializeField] private Transform hand;
    [SerializeField] private TutorialID idTutorialNextStep;
    private Board boardGame => InGameManager.Instance.Board;
    private BlockSpawnerManager spawnerGame => InGameManager.Instance.SpawnerManager;
    private BooterManager booterManager => BooterManager.Instance;
    private List<Cell> lstTarget;
    bool isFinish = false;
    private Sequence sequance;
    private void OnEnable() {
        EventDispatcher.AddListener<EventKey.EventInputSelect>(ListenInPut);
        EventDispatcher.AddListener<EventKey.BoardChange>(ListenBoardChange);
    }

    private void OnDisable() {
        EventDispatcher.RemoveListener<EventKey.EventInputSelect>(ListenInPut);
        EventDispatcher.RemoveListener<EventKey.BoardChange>(ListenBoardChange);
        DOTween.Kill(sequance);
    }

    private void ListenBoardChange(EventKey.BoardChange evt) {
        if(isFinish) {
            if(idTutorialNextStep == TutorialID.NONE) {
                DataManager.Instance.PlayerData.Tutorial = true;
                booterManager.UnSelect();
                Destroy(TutorialManager.Instance.gameObject);
            } else {
                TutorialManager.Instance.ShowTutorial(idTutorialNextStep);
            }
        }
    }

    private void ListenInPut(EventKey.EventInputSelect evt) {
        if(evt.Select) {
            if(evt.IPlayerObject.I_Transform == spawnerGame.LstBlockSpawner[1].transform) {
                hand.gameObject.SetActive(false);
            }
        } else {
            if(evt.IPlayerObject.I_Transform == spawnerGame.LstBlockSpawner[1].transform) {
                List<Cell> lstRelease = spawnerGame.LstBlockSpawner[1].LstCell_Plance;
                bool correct = true;
                foreach(var cell in lstTarget) {
                    if(!lstRelease.Contains(cell)) {
                        correct = false;
                        break;
                    }
                }
                if(correct) {
                    isFinish = true;
                } else {
                    foreach(var cell in spawnerGame.LstBlockSpawner[1].LstCell_Plance) {
                        cell.SetStatus(Cell.Status.NORMAL);
                    }
                    spawnerGame.LstBlockSpawner[1].LstCell_Plance.Clear();
                    hand.gameObject.SetActive(true);
                    SetUpHand();
                }
            }
        }
    }


    public override void Show(Action callBack = null) {
        lstTarget = new List<Cell>();
        foreach(var coor in lstIndexTarget) {
            lstTarget.Add(boardGame.GetCellByIndex(coor.m_X, coor.m_Y));
        }
        SetUpMap();
        SetUpSpawner();
        SetUpHand();
        booterManager.OnSelect(null);
        isFinish = false;
    }
    private void SetUpMap() {
        InGameManager.Instance.Board.Show();
        foreach(var coor in lstCoor) {
            Cell cell = boardGame.GetCellByIndex(coor.m_X,coor.m_Y);
            cell.Add(MaterialID.BLOCKNORMAL);
        }
    }
    
    private void SetUpSpawner() {
        spawnerGame.Show();
        spawnerGame.LstBlockSpawner[0].Block.Show(null);
        spawnerGame.LstBlockSpawner[1].Block.Show(blockSave);
        spawnerGame.LstBlockSpawner[2].Block.Show(null);
    }

    private void SetUpHand() {
        Vector3 target_Start = spawnerGame.LstBlockSpawner[1].transform.position;
        Vector3 startPosition = target_Start + new Vector3(1,-1);
        Vector3 target_End = lstTarget[1].transform.position;
        hand.position = startPosition;
        hand.localScale = Vector3.one;
        DOTween.Kill(sequance);
        Sequence sequence = DOTween.Sequence();
        sequence.Append(hand.DOMove(target_Start, 1f));
        sequence.Append(hand.DOScale(Vector3.one*0.9f, 0.5f).SetEase(Ease.OutBack));
        sequence.Append(hand.DOMove(target_End, 1.5f));
        sequence.Append(hand.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack));
        sequence.Append(hand.DOMove(startPosition, 1.5f));
        sequence.SetLoops(-1);
    }


    private void OnDestroy() {
        DOTween.Kill(sequance);
    }
}
