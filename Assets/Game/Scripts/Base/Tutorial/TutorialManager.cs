using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TutorialManager : SingletonResource<TutorialManager> {
    public const string SORTING_LAYER_NAME = "Tutorial";
    public const int SORTING_Order = 0;
    [SerializeField] private Canvas canvas;
    [SerializeField] private TutorialBase[] tutorials;
    [SerializeField] private Transform root;
    private TutorialBase curTutorial;
    public bool HasTutorial => curTutorial != null;
    protected override void OnAwake() {
        base.OnAwake();
        SetUpCanvas();
    }

    private void Start() {
        //canvas.worldCamera = Camera.main;
        //canvas.sortingLayerName = SORTING_LAYER_NAME;
        //canvas.sortingOrder = SORTING_Order;
    }

    private TutorialBase GetTutorial(TutorialID tutorialID) {
        foreach(TutorialBase tutorial in tutorials) {
            if(tutorial.TutoriaID == tutorialID)
                return tutorial;
        }
        return null;
    }

    public void ShowTutorial(TutorialID tutorialID, Action onCompleted = null) {
        if(curTutorial != null) {
            Destroy(curTutorial.gameObject);
            curTutorial = null;
        }
        TutorialBase tutorialPref = GetTutorial(tutorialID);
        if(!tutorialPref) {
            Debug.LogWarning($"[TutorialManager] Show tutorial {tutorialID} failed! Tutorialno found.");
            return;
        }
        TutorialBase tutorial = Instantiate(tutorialPref, root);
        ;
        tutorial.Show(() => {
            Destroy(curTutorial.gameObject);
            curTutorial = null;
            onCompleted?.Invoke();
        });
        curTutorial = tutorial;
    }

    public void SetUpCanvas() {
        canvas.worldCamera = Camera.main;
        canvas.sortingLayerName = SORTING_LAYER_NAME;
        canvas.sortingOrder = SORTING_Order;
    }
}