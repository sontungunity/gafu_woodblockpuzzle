
using STU;
using UnityEngine;

public static class EventKey {
    public struct LoadFinal : IEventArgs {

    }

    public struct SceneChange : IEventArgs {
        public string nameScene;

        public SceneChange(string nameScene) {
            this.nameScene = nameScene;
        }
    }

    public struct BoardChange : IEventArgs {

    }

    public struct BlockChange : IEventArgs {

    }

    public struct ScoreChange : IEventArgs {

    }

    public struct HightScore : IEventArgs {

    }
    public struct IteamChange : IEventArgs {
        public ItemID itemID;
        public int curAmount;
        public int changeAmount;

        public IteamChange(ItemID itemID, int curAmount, int changeAmount) {
            this.itemID = itemID;
            this.curAmount = curAmount;
            this.changeAmount = changeAmount;
        }
    }

    public struct GetMaterial : IEventArgs {
        public MaterialBase Material;

        public GetMaterial(MaterialBase material) {
            this.Material = material;
        }
    }

    public struct GetItemFromUI : IEventArgs {
        public ItemStack ItemStack;
        public Vector3 Position;


        public GetItemFromUI(ItemStack itemStack, Vector3 position) {
            this.ItemStack = itemStack;
            this.Position = position;
        }
    }

    public struct Vibration : IEventArgs {
        public int level; //0 -> 5
        public Vibration(int level) {
            this.level = level;
        }
    }

    public struct EventGameStatus : IEventArgs {
        public InGameManager.GameStatus Status;

        public EventGameStatus(InGameManager.GameStatus status) {
            this.Status = status;
        }
    }

    public struct EventInputSelect : IEventArgs {
        public bool Select;
        public IPlayerInput IPlayerObject;
        public Vector3 Position;

        public EventInputSelect(bool select, IPlayerInput iPlayerObject, Vector3 position) {
            this.Select = select;
            this.IPlayerObject = iPlayerObject;
            this.Position = position;
        }
    }
}
